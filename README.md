# Melanomenet website project

## Common info

Backend on [Strapi.io](https://strapi.io/)
> Strapi is a free and open-source headless CMS.

Frontend on [Gatsbyjs](https://www.gatsbyjs.com/)
> Gatsby is a free and open source framework based on React that helps developers build blazing fast websites and apps.

It used *webhoohs* to update frontend, *graphql* to fetch them.

**Used packages:**

- [node](https://nodejs.org/) (available in package manager);
- [yarnpkg](https://yarnpkg.com/) (available in package manager);
- [ffmpeg](https://ffmpeg.org/) (available in package manager);
- [webhooks](https://github.com/adnanh/webhook) (available in package manager);
- [rsync](https://rsync.samba.org/) (available in package manager);

## System (debian / ubuntu)

```bash
apt install webhooks yarnpkg node ffmpeg rsync
```

> I'd also prefer to use [screen](https://www.gnu.org/software/screen/) for save sessions.

## Webhooks

**Configs** in `./webhooks`

```bash
webhook -hooks webhooks/hooks.json -verbose
```

## Backend (strapi)

**Install:**

```bash
cd strapi
yarnpkg install
yarnpkg build
```

**Run:**

```bash
cd strapi
export NODE_ENV=production
yarnpkg start
```

## Frontend (gatsby)

- [typescript](https://www.typescriptlang.org/)
- [graphql](https://graphql.org/) - by [gatsby](https://www.gatsbyjs.com/docs/graphql/)

**Install:**

```bash
cd frontend
yarnpkg install
```

**Run:**

> Rebuild script is on `./frontend/rebuild`

```bash
cd frontend
export NODE_ENV=production
yarnpkg clean
yarnpkg build

# Update working directory for prevent empty page
# when gatsby in rebuild frontend

rsync -a --delete /srv/melanomenet/website/frontend/public/ /srv/melanomenet/www/
```

## Notes

- **FFmpeg crash** on build when videos converting with auto sizes (eg. `-1:1080`);
- Nginx going to another directory for prevent empty page when frontend on build, it takes more disk space;
