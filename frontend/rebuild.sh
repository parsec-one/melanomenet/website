#!/bin/bash
export SITE_URL=https://melanomenet.ru
export NODE_ENV=production

echo "Start updating"
echo "Clean gatsby cache"

yarnpkg clean 

echo "Start build"

yarnpkg build

echo "Finish build"
echo "Start coping files"

rsync -a --delete /home/melanomenet/website/frontend/public/ /home/melanomenet/www/

echo "Finish coping"
