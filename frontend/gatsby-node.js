const {
  DefaultTypes,
  StrapiArticles,
  StrapiBoardTeams,
  StrapiPrograms,
  StrapiTags,
  StrapiWikiItem,
  StrapiWikis,
  // Pages
  StrapiAboutUs,
  StrapiBoardPage,
  StrapiContactsPage,
  StrapiDocsPage,
  StrapiHomePage,
  StrapiNewsPage,
  StrapiNotFound,
  StrapiProgramPage,
  StrapiSpecialistsPage,
  StrapiWikipage,
  // Single items
  StrapiFoundCreatorWidget,
  // Site info
  StrapiContacts,
  StrapiSiteInfo,
} = require('./shemaTypes');
const path = require('path')
const { createFilePath, createRemoteFileNode } = require('gatsby-source-filesystem');

let siteURL = '';

exports.onCreateNode = async ({ node, actions, getNode, store, cache, createNodeId }) => {
  const { createNode, createNodeField } = actions;
  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  };

  if (node.internal.type === `StrapiSiteInfo`) {
    siteURL = node.site_url;
  }

  const addLocalFile = async (filesList) => {
    const files = await Promise.all(
      filesList.map(file =>
        createRemoteFileNode({
          url: `${process.env.GATSBY_API_URL || 'http://localhost:1337'}${file.url}`,
          parentNodeId: node.id,
          store,
          cache,
          createNode,
          createNodeId,
        })
      )
    )

    filesList.forEach((file, index) => {
      file.localFile___NODE = files[index].id
    })
  }

  if (['StrapiHomePage', 'StrapiWikiPage', 'StrapiBoardPage'].includes(node.internal.type)) {
    await addLocalFile(node.background);
  }

  if (['StrapiFoundCreatorWidget'].includes(node.internal.type)) {
    await addLocalFile([node.image]);
  }

  if (['StrapiArticles', 'StrapiWikis', 'StrapiBoardTeams', 'StrapiProgram'].includes(node.internal.type)) {
    if (Array.isArray(node.image)) {
      await addLocalFile(node.image);
    }
  }

  if (['StrapiDocs', 'StrapiBoardTeams', 'StrapiProgram'].includes(node.internal.type)) {
    if (Array.isArray(node.files)) {
      await addLocalFile(node.files);
    }
  }

  if (['StrapiTags'].includes(node.internal.type)) {
    for (const wiki of node.wikis) {
      if (Array.isArray(wiki.image)) {
        await addLocalFile(wiki.image);
      }
    }
  }
};

/**
 * Genetate news pages
 * @param {*} param0 
 */

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const newsComponent = path.resolve('./src/templates/news/index.tsx');
  const articleComponent = path.resolve('./src/templates/articlePage/index.tsx');

  const result = await graphql(`
    {
      allStrapiArticles(
        sort: { fields: created_at, order: DESC }
        limit: 10000
      ) {
        edges {
          node {
            slug
          }
        }
        totalCount
      }

      allStrapiWikis(
        sort: { fields: created_at, order: DESC }
        limit: 10000
      ) {
        edges {
          node {
            slug
          }
        }
      }

      allStrapiTags(
        sort: { fields: created_at, order: DESC }
        limit: 10000
      ) {
        edges {
          node {
            slug
          }
        }
      }

      allStrapiBoardTeams(
        sort: { fields: created_at, order: DESC }
        limit: 10000
      ) {
        edges {
          node {
            slug
          }
        }
      }

      allStrapiProgram(
        sort: { fields: created_at, order: DESC }
        limit: 10000
      ) {
        edges {
          node {
            slug
          }
        }
      }

      strapiSiteInfo {
        title
        previewImage {
          childImageSharp {
            resize(width: 1920) {
              src
            }
          }
        }
        site_url
      }
    }
  `);

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }

  const { strapiSiteInfo } = result.data;

  const { edges, totalCount } = result.data.allStrapiArticles;

  const postsPerPage = 6;
  const numPages = Math.ceil(totalCount / postsPerPage);

  Array.from({ length: numPages }).forEach((_, index) => {
    createPage({
      path: index === 0 ? `/news` : `/news/${index + 1}`,
      component: newsComponent,
      context: {
        skip: index * postsPerPage,
        limit: postsPerPage,
        numPages,
        currentPage: index + 1,
        siteInfo: strapiSiteInfo,
      },
    })
  })

  edges.forEach(({ node: { slug } }, index) => {
    createPage({
      path: `/news/${slug}`,
      component: articleComponent,
      context: {
        slug: slug,
        siteInfo: strapiSiteInfo,
      },
    })
  })

  /**
   * Wiki Pages
   */

  const wikiComponent = path.resolve('./src/templates/wikiPage/index.tsx');

  const { edges: wikiEdges } = result.data.allStrapiWikis;

  wikiEdges.forEach(({ node: { slug } }, index) => {
    createPage({
      path: `/wiki/${slug}`,
      component: wikiComponent,
      context: {
        slug: slug,
        siteInfo: strapiSiteInfo,
      },
    })
  })

  /**
   * Wiki tag page
   */

  const wikiTagComponent = path.resolve('./src/templates/wikiTagPage/index.tsx');

  const { edges: tagEdges } = result.data.allStrapiTags;

  tagEdges.forEach(({ node: { slug } }, index) => {
    createPage({
      path: `/${slug}`,
      component: wikiTagComponent,
      context: {
        slug: slug,
        siteInfo: strapiSiteInfo,
      },
    })
  })

  /**
   * Board teams pages
   */

  const boardMemberComponent = path.resolve('./src/templates/boardMember/index.tsx');

  const { edges: boardEdges } = result.data.allStrapiBoardTeams;

  boardEdges.forEach(({ node: { slug } }, index) => {
    createPage({
      path: `/${slug}`,
      component: boardMemberComponent,
      context: {
        slug: slug,
        siteInfo: strapiSiteInfo,
      },
    })
  })

  /**
   * Programs pages
   */

  const programComponent = path.resolve('./src/templates/program/index.tsx');

  const { edges: programEdges } = result.data.allStrapiProgram;

  programEdges.forEach(({ node: { slug } }, index) => {
    createPage({
      path: `/programs/${slug}`,
      component: programComponent,
      context: {
        slug: slug,
        siteInfo: strapiSiteInfo,
      },
    })
  })
};

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    ${DefaultTypes}

    type StrapiArticles implements Node {
      ${StrapiArticles}
    }

    type StrapiWikis implements Node {
      ${StrapiWikis}
    }

    type StrapiWikiItem implements Node {
      ${StrapiWikiItem}
    }

    type StrapiTagsWikis implements Node {
      ${StrapiWikiItem}
    }

    type StrapiBoardTeams implements Node {
      ${StrapiBoardTeams}
    }

    type StrapiHomePage implements Node {
      ${StrapiHomePage}
    }

    type StrapiAboutUs implements Node {
      ${StrapiAboutUs}
    }

    type StrapiNewsPage implements Node {
      ${StrapiNewsPage}
    }

    type StrapiWikiPage implements Node {
      ${StrapiWikipage}
    }

    type StrapiContactsPage implements Node {
      ${StrapiContactsPage}
    }

    type StrapiBoardPage implements Node {
      ${StrapiBoardPage}
    }

    type StrapiProgramPage implements Node {
      ${StrapiProgramPage}
    }

    type StrapiDocs implements Node {
      ${StrapiDocsPage}
    }

    type StrapiSpecialistsPage implements Node {
      ${StrapiSpecialistsPage}
    }

    type StrapiNotFound implements Node {
      ${StrapiNotFound}
    }

    type StrapiSiteInfo implements Node {
      ${StrapiSiteInfo}
    }

    type StrapiTags implements Node {
      ${StrapiTags}
    }

    type StrapiFoundCreatorWidget implements Node {
      ${StrapiFoundCreatorWidget}
    }

    type StrapiContacts implements Node {
      ${StrapiContacts}
    }

    type StrapiProgram implements Node {
      ${StrapiPrograms}
    }
  `
  createTypes(typeDefs)
}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /react-leaflet/,
            use: loaders.null(),
          },
          {
            test: /leaflet/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}
