interface OriginalPositionsInterface {
  top: number,
  width: number,
}

interface ScrollPanelInterface {
  topAnchor: HTMLElement,
  target: HTMLElement,
  originalPositions: OriginalPositionsInterface,
  translateY: number,
  bottomAnchor: HTMLElement | null,
}

const ScrollPanel = ({
  topAnchor,
  target,
  originalPositions,
  translateY,
  bottomAnchor,
}: ScrollPanelInterface): void => {
  const topAnchorBottom = topAnchor.getBoundingClientRect().top + topAnchor.offsetHeight;
  const { top: targetTop, bottom: targetBottom } = target.getBoundingClientRect();

  if (window.pageYOffset < (originalPositions.top - topAnchorBottom - 20)) {
    target.removeAttribute('style');
    target.classList.remove('fixed');
  } else if ((topAnchorBottom - targetTop) > -40) {
    target.style.position = 'fixed';
    target.style.top = '0px';
    translateY = topAnchorBottom + 20;

    target.style.transform = `translateY(${translateY}px)`;
    target.classList.add('fixed');
  }

  if (bottomAnchor) {
    const bottomAnchorTop = bottomAnchor?.getBoundingClientRect().top || 0;

    if (
      bottomAnchorTop <= targetBottom + 40 &&
      bottomAnchorTop - topAnchorBottom - 40 <= target.offsetHeight
    ) {
      const qw = bottomAnchorTop - targetBottom - topAnchorBottom + targetTop - 40;
      target.style.transform = `translateY(${translateY + qw}px)`;
    }
  }
}

/**
 * Fix HTML element to anchor from top and bottomm
 * but scroll with page
 * 
 * @param  {String} targetElement   Target element query
 * @param  {String} topElement      Top element anchor
 * @param  {String} bottomElement   Bottom element anchor
 * @return {void}                   Should change class
*/

const panelFixedScroll = (
  targetElement: string,
  topElement: string,
  bottomElement: string,
): void => {
  if (window.screen.width <= 992) return;

  const target: HTMLElement | null = document.querySelector(targetElement);
  const topAnchor: HTMLElement | null = document.querySelector(topElement);

  const originalPositions = {
    top: (Number(target?.getBoundingClientRect().top) + window.pageYOffset) || 0,
    width: target?.offsetWidth || 0,
  }

  if (!target || !topAnchor) {
    console.warn('target or top anchor not found');
    return;
  };

  const bottomAnchor: HTMLElement | null = document.querySelector(bottomElement);

  let translateY = 0;

  document.addEventListener('scroll', () => ScrollPanel({
    topAnchor,
    target,
    originalPositions,
    translateY,
    bottomAnchor,
  }));
}

const scrollPanelInit = () => {
  if (window.screen.width > 992) {
    panelFixedScroll('#panel-right', 'header', 'footer');
  }
}

// scrollPanelInit();
