var ScrollPanel = function (_a) {
    var topAnchor = _a.topAnchor, target = _a.target, originalPositions = _a.originalPositions, translateY = _a.translateY, bottomAnchor = _a.bottomAnchor;
    var topAnchorBottom = topAnchor.getBoundingClientRect().top + topAnchor.offsetHeight;
    var _b = target.getBoundingClientRect(), targetTop = _b.top, targetBottom = _b.bottom;
    if (window.pageYOffset < (originalPositions.top - topAnchorBottom - 20)) {
        target.removeAttribute('style');
        target.classList.remove('fixed');
    }
    else if ((topAnchorBottom - targetTop) > -40) {
        target.style.position = 'fixed';
        target.style.top = '0px';
        translateY = topAnchorBottom + 20;
        target.style.transform = "translateY(" + translateY + "px)";
        target.classList.add('fixed');
    }
    if (bottomAnchor) {
        var bottomAnchorTop = (bottomAnchor === null || bottomAnchor === void 0 ? void 0 : bottomAnchor.getBoundingClientRect().top) || 0;
        if (bottomAnchorTop <= targetBottom + 40 &&
            bottomAnchorTop - topAnchorBottom - 40 <= target.offsetHeight) {
            var qw = bottomAnchorTop - targetBottom - topAnchorBottom + targetTop - 40;
            target.style.transform = "translateY(" + (translateY + qw) + "px)";
        }
    }
};
/**
 * Fix HTML element to anchor from top and bottomm
 * but scroll with page
 *
 * @param  {String} targetElement   Target element query
 * @param  {String} topElement      Top element anchor
 * @param  {String} bottomElement   Bottom element anchor
 * @return {void}                   Should change class
*/
var panelFixedScroll = function (targetElement, topElement, bottomElement) {
    if (window.screen.width <= 992)
        return;
    var target = document.querySelector(targetElement);
    var topAnchor = document.querySelector(topElement);
    var originalPositions = {
        top: (Number(target === null || target === void 0 ? void 0 : target.getBoundingClientRect().top) + window.pageYOffset) || 0,
        width: (target === null || target === void 0 ? void 0 : target.offsetWidth) || 0
    };
    if (!target || !topAnchor) {
        console.warn('target or top anchor not found');
        return;
    }
    ;
    var bottomAnchor = document.querySelector(bottomElement);
    var translateY = 0;
    document.addEventListener('scroll', function () { return ScrollPanel({
        topAnchor: topAnchor,
        target: target,
        originalPositions: originalPositions,
        translateY: translateY,
        bottomAnchor: bottomAnchor
    }); });
};
var scrollPanelInit = function () {
    if (window.screen.width > 992) {
        panelFixedScroll('#panel-right', 'header', 'footer');
    }
};
// scrollPanelInit();
