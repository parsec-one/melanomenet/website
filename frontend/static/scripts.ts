"use strict";

const menuScroll = () => {
  const menu = document.querySelector('header');

  if (!menu) return;

  const { offsetHeight } = menu;

  if (window.scrollY > offsetHeight) {
    document.querySelector('header')?.classList.add('scrolled');
  } else {
    menu.classList.remove('scrolled');
  }
}

const menuScrollInit = () => {
  document.addEventListener('scroll', menuScroll);
  menuScroll();
}

menuScrollInit();
