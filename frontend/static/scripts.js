"use strict";
var menuScroll = function () {
    var _a;
    var menu = document.querySelector('header');
    if (!menu)
        return;
    var offsetHeight = menu.offsetHeight;
    if (window.scrollY > offsetHeight) {
        (_a = document.querySelector('header')) === null || _a === void 0 ? void 0 : _a.classList.add('scrolled');
    }
    else {
        menu.classList.remove('scrolled');
    }
};
var menuScrollInit = function () {
    document.addEventListener('scroll', menuScroll);
    menuScroll();
};
menuScrollInit();
