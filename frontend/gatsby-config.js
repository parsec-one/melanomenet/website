const fs = require('fs');
const fetch = require("node-fetch");
const gracefulFs = require('graceful-fs');
gracefulFs.gracefulify(fs);

/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */
const path = require('path')
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
});

const {
  NODE_ENV,
  SITE_URL = 'http://127.0.0.1:8000',
  GATSBY_API_URL = 'http://127.0.0.1:1337',
} = process.env;

const siteInfoAnalytics = async () => {
  const response = await fetch(`${GATSBY_API_URL}/site-info`);
  const siteInfo = response.ok ? await response.json() : null;
  const analytics = siteInfo ? siteInfo.analytics : {};

  return analytics || {};
};

module.exports = {
  siteMetadata: {
    title: `Melanomenet`,
    siteUrl: SITE_URL,
  },
  plugins: [
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-typescript',
    'gatsby-plugin-image',
    'gatsby-image',
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        resolveEnv: () => NODE_ENV,
        env: {
          production: {
            policy: [{ userAgent: '*' }],
          },
          'beta': {
            policy: [{ userAgent: '*', disallow: ['/'] }],
            sitemap: null,
            host: null,
          },
        },
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        sitemapSize: 5000
      }
    },
    {
      resolve: 'gatsby-source-strapi',
      options: {
        apiURL: GATSBY_API_URL,
        contentTypes: [
          'articles',
          'menus',
          'tags',
          'wikis',
          'board-teams',
          'program',
        ],
        singleTypes: [
          'about-us',
          'contacts',
          'contacts-page',
          'docs',
          'home-page',
          'news-page',
          'not-found',
          'site-info',
          'wiki-page',
          'board-page',
          'program-page',
          'specialists-page',
          'found-creator-widget',
        ],
        markdownImages: {
          typesToParse: {
            articles: ['content'],
            wikis: ['content'],
            'about-us': ['content'],
          }
        },
        queryLimit: 5000,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/assets/images`,
      },
    },
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        'UI': path.join(__dirname, 'src/UI'),
        'src': path.join(__dirname, 'src'),
        'components': path.join(__dirname, 'src/components'),
        'pages': path.join(__dirname, 'src/pages'),
        'interfaces': path.join(__dirname, 'src/interfaces'),
        'templates': path.join(__dirname, 'src/templates'),
        'utils': path.join(__dirname, 'src/utils'),
      }
    },
    {
      resolve: `gatsby-plugin-webfonts`,
      options: {
        fonts: {
          google: [
            {
              family: "Open Sans",
              variants: ["300", "400", "500", "600", "700"],
              subsets: ['latin', 'cyrillic'],
              strategy: 'selfHosted',
            },
          ],
        },

        formats: ['woff2', 'ttf'],
        //useMinify: true,
        //usePreload: true,
        //usePreconnect: false,
      },
    },
    {
      resolve: `gatsby-transformer-video`,
      options: {
        downloadBinaries: false,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'G-QL1BBD8P0E',
        // head: false,
        // anonymize: true,
        // respectDNT: true,
        // Avoids sending pageview hits from custom paths
        // exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Delays sending pageview hits on route update (in milliseconds)
        //pageTransitionDelay: 0,
        // Defers execution of google analytics script after page load
        // defer: false,
        // Any additional optional fields
        // sampleRate: 5,
      },
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: 'GTM-MM853MT',
        includeInDevelopment: false,
        defaultDataLayer: { platform: "gatsby" },
      },
    },
  ],
};
