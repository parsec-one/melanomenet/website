/**
 * Query context
 */

export interface QueryContextInterface {
  currentPage: number;
  limit: number;
  numPages: number;
  skip: number;
}

/**
 * News interfaces
 */

export interface ResizeImageInterface {
  src?: string;
  width?: number;
  height?: number;
}

export interface FluidImageInterface {
  src?: string;
  base64?: string;
  srcWebp?: string;
  width?: number;
  srcSet: string;
  srcSetWebp: string;
  publicURL?: string;
}

export interface ChildImageSharpInterface {
  fluid?: FluidImageInterface;
  resize?: ResizeImageInterface;
  resolutions?: ResizeImageInterface;
}

export interface VideoFileInterface {
  path?: string;
  absolutePath?: string;
  name?: string;
  ext?: string;
  codec?: string;
  formatName?: string;
  formatLongName?: string;
  startTime?: number;
  duration?: number;
  size?: number;
  bitRate?: number;
}

export interface ImageFileInterface {
  childImageSharp?: ChildImageSharpInterface;
  width?: number;
  height?: number;
  size?: number;
  publicURL?: string;
  url?: string;
  uid?: number;
  videoScreenshots?: {
    publicURL: string;
  }[];
  videoH264?: VideoFileInterface;
  videoVP9?: VideoFileInterface;
}

export interface StrapiMediaFileInterface {
  ext?: string;
  mime?: string;
  name?: string;
  url?: string;
  width?: number;
  size?: number;
  updated_at?: Date;
  id?: number;
  alternativeText?: string;
  caption?: string;
  localFile?: ImageFileInterface;
}

export interface NewsItemInterface {
  title: string;
  description: string;
  created_at: string;
  image?: StrapiMediaFileInterface[];
  slug: string;
  text?: string;
}

export interface StrapiNodeInerface {
  node: NewsItemInterface;
}

export interface AllStrapiArticlesInterface {
  edges: Array<StrapiNodeInerface>;
  totalCount: number;
}

/**
 * Board teams interface
 */

export interface BoardMemberInterface {
  id?: number;
  name: string;
  description?: string;
  image: StrapiMediaFileInterface[];
  files?: StrapiMediaFileInterface[];
  content?: string;
  slug: string;
  created_at?: string;
  isPresident?: boolean;
}

export interface BoardTeamsInterface {
  edges: {
    node: BoardMemberInterface[];
  };
  nodes: BoardMemberInterface[];
}

/**
 * Programs interface
 */

export interface ProgramItemInterface {
  id?: number;
  name: string;
  description?: string;
  image: StrapiMediaFileInterface[];
  files?: StrapiMediaFileInterface[];
  content?: string;
  slug: string;
  created_at?: string;
}

export interface ProgramsInterface {
  edges: {
    node: ProgramItemInterface[];
  };
  nodes: ProgramItemInterface[];
}

/**
 * Wiki page interface
 */

export interface WikiItemInterface {
  title: string;
  description: string;
  content: string;
  slug: string;
  tags?: Array<{
    name: string;
    slug: string;
  }>;
  image?: StrapiMediaFileInterface[];
  created_at: string;
}

export interface AllStrapiWikisInterface {
  edges: {
    node: WikiItemInterface;
  }[];
  nodes: WikiItemInterface[];
  totalCount?: number;
}

export interface WikiTagInterface {
  id: number;
  name: string;
  slug: string;
  wikis?: Array<WikiItemInterface>;
}

export interface AllWikiTagsInterface {
  edges: Array<{
    node: WikiTagInterface;
  }>;
  totalCount?: number;
}

/**
 * Contacts interfaces
 */

export interface ContactsLocationInterface {
  address: string;
  link?: string;
}

export interface ContactsInteface {
  phone: string;
  email: string;
  location?: Array<ContactsLocationInterface>;
}

/**
 * Navigation interface
 */

export interface SiteInfoInterface {
  logo: ImageFileInterface;
  logoText: string;
  title?: string;
  site_url?: string;
  previewImage?: {
    childImageSharp: ChildImageSharpInterface;
  };
  favicon?: {
    childImageSharp: ChildImageSharpInterface;
  };
  analytics?: {
    googleTagId: string;
    googleAnalyticsId: string;
  };
}

export interface StrapiMenuPositionInterface {
  isFooter: boolean;
  isHeader: boolean;
  isMobile: boolean;
}

export interface StrapiPagesItemInterface {
  tags?: WikiTagInterface[];
}

export interface StrapiMenuItemInterface {
  position: StrapiMenuPositionInterface;
  name: string;
  link: string;
  id: number;
  submenu?: StrapiMenuItemInterface[];
  pages?: StrapiPagesItemInterface[];
}

export interface GatsbyMenuItemInterface {
  position: StrapiMenuPositionInterface;
  name: string;
  link: string;
  id: number;
  submenu?: StrapiMenuItemInterface[];
}

export interface StrapiMenuInterface {
  nodes: StrapiMenuItemInterface[];
}

/**
 * Page interface
 */

export interface PageMetaInterface {
  title: string;
  description: string;
}

export interface PageInterface {
  content?: string;
  background?: StrapiMediaFileInterface[];
  files?: StrapiMediaFileInterface[];
  meta: PageMetaInterface;
}
