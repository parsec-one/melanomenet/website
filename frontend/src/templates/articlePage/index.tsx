import React, { FC } from 'react'
import { graphql, PageProps } from 'gatsby'
import { FiCalendar } from 'react-icons/fi';

import { NewsItemInterface } from 'interfaces/interfaces'

import { Section, HR, Flex, Container } from 'UI'
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs'

import { Article, ContactUs, Layout, MediaSlider, PageHeader } from 'components'
import LastNewsPanel from 'components/News/LastNewsPanel';

interface ArticlePageInterface extends PageProps {
  data: {
    strapiArticles: NewsItemInterface;
  }
}

const ArticlePageTemplate: FC<ArticlePageInterface> = ({
  data: { strapiArticles },
  path,
}) => {
  const {
    title,
    description,
    text = '',
    slug,
    image,
    created_at: createdAt,
  } = strapiArticles;

  const pageInfo = {
    meta: {
      title,
      description,
    },
    previewImageUrl: image?.[0]?.localFile?.childImageSharp?.resolutions?.src,
  }

  const dateTime = new Date(createdAt);
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  const dateText = dateTime.toLocaleDateString('ru-RU', dateOptions);

  return (
    <Layout pageInfo={pageInfo} path={path}>
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/news">Новости</BreadcrumbsItem>
          <BreadcrumbsItem to={`/news/${slug}`}>{title}</BreadcrumbsItem>
        </Breadcrumbs>

        <Flex space={30} justifyContent="space-between" alignItems="stretch">
          <Section title={title}>
            <HR />
            {image && <MediaSlider files={image} />}
            <Article source={text} />
            <HR />
            <Flex space={10} alignItems="center">
              <FiCalendar />
              <span>{ dateText }</span>
            </Flex>
          </Section>

          <LastNewsPanel />
        </Flex>
      </Container>

      <ContactUs />
    </Layout>
  )
}

export default ArticlePageTemplate;

export const pageQuery = graphql`
  query articleItemQuery($slug: String!) {
    strapiArticles(
      slug: { eq: $slug }
    ) {
      title
      description
      text
      image {
        alternativeText
        caption
        localFile {
          childImageSharp {
            fluid(
              maxWidth: 640
            ) {
              src
              srcSet
              srcSetWebp
            }
          }
          publicURL
        }
      }
      slug
      created_at
    }
  }
`
