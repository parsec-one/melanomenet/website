import React, { FC } from 'react';
import { graphql, PageProps } from 'gatsby';

import { ProgramItemInterface } from 'interfaces/interfaces';

import { Section, HR, Flex, Container } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { Article, Layout, MediaSlider, PageHeader, ContactPanel } from 'components';

interface ProgramPageInterface extends PageProps {
  data: {
    strapiProgram: ProgramItemInterface;
  }
}

const programTemplate: FC<ProgramPageInterface> = ({
  data: { strapiProgram },
  path,
}) => {
  const {
    name,
    description,
    content = '',
    slug,
    image,
  } = strapiProgram;

  const pageInfo = {
    meta: {
      title: name,
      description,
    },
    previewImageUrl: image?.[0]?.localFile?.childImageSharp?.resolutions?.src,
  }

  return (
    <Layout pageInfo={pageInfo} path={path}>
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/programs">Программы фонда</BreadcrumbsItem>
          <BreadcrumbsItem to={`/${slug}`}>{name}</BreadcrumbsItem>
        </Breadcrumbs>

        <Flex space={30} justifyContent="space-between" alignItems="stretch">
          <Section title={name}>
            <HR />
            {image && <MediaSlider width={240} files={image} />}
            <Article source={content} />
          </Section>
          <ContactPanel />
        </Flex>
      </Container>
    </Layout>
  );
}

export default programTemplate;

export const pageQuery = graphql`
  query programQuery($slug: String!) {
    strapiProgram(
      slug: { eq: $slug }
    ) {
      name
      description
      content
      image {
        alternativeText
        caption
        localFile {
          childImageSharp {
            fluid(
              maxWidth: 640
            ) {
              src
              srcSet
              srcSetWebp
            }
            resolutions(width: 1920) {
              src
            }
          }
          publicURL
        }
      }
      slug
      created_at
    }
  }
`;
