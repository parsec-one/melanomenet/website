import styled from 'styled-components';

import { Flex } from 'UI';
import { screen } from 'UI/variables';

export const Headers = styled(Flex)`
  display: none;

  @media ${screen.tablet} {
    display: block;
  }
`;