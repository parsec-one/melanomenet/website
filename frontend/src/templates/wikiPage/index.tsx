import React, { FC } from 'react';
import { graphql, PageProps } from 'gatsby';
import { FiCalendar } from 'react-icons/fi';

import { SiteInfoInterface, WikiItemInterface } from 'interfaces/interfaces';
import { getFirstImageLink } from 'utils/resize';

import { WikiHeaders } from 'components/Wikis';
import { Section, HR, Flex, Container } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { PageHeader, Layout, WikiArticle, WikiPanel, MediaSlider, ContactUs } from 'components';

import { Headers } from './styles';

interface WikiPageInterface extends PageProps {
  data: {
    strapiWikis: WikiItemInterface;
  }
  pageContext: {
    slug: string;
    siteInfo: SiteInfoInterface;
  }
}

const WikiPageTemplate: FC<WikiPageInterface> = ({
  data: { strapiWikis },
  path,
  pageResources,
  pageContext,
}) => {
  const {
    title,
    description = '',
    content = '',
    image = [],
    tags = [{ name: '', slug: '' }],
    created_at: createdAt,
  } = strapiWikis;

  const firstImage = image?.[0]?.localFile;

  const pageInfo = {
    meta: {
      title,
      description,
    },
    previewImageUrl: getFirstImageLink(image),
  }

  const dateTime = new Date(createdAt);
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  const dateText = dateTime.toLocaleDateString('ru-RU', dateOptions);

  return (
    <Layout pageInfo={pageInfo} path={path}>
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/wiki">О меланоме</BreadcrumbsItem>
          <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
        </Breadcrumbs>
        <Flex space={30} justifyContent="space-between" alignItems="stretch">
          <Section title={title}>
            <HR />
            <Headers column space={20}>
              <h3>Содержание:</h3>
              <WikiHeaders path={path} content={content} />
            </Headers>
            {image && <MediaSlider files={image} />}
            <WikiArticle
              source={content}
            />
            <HR />
            <Flex space={10} alignItems="center">
              <FiCalendar />
              <span>{ dateText }</span>
            </Flex>
          </Section>
          <WikiPanel path={path} content={content} />
        </Flex>
      </Container>

      <ContactUs />
    </Layout>
  )
}

export default WikiPageTemplate;

export const pageQuery = graphql`
  query wikiItemQuery($slug: String!) {
    strapiWikis(
      slug: { eq: $slug }
    ) {
      ...StrapiWikisFragment
    }
  }

  fragment StrapiWikisFragment on StrapiWikis {
    title
    content
    description
    tags {
      name
      slug
    }
    created_at
    image {
      ...StrapiSingleFileFragment
    }
  }
`
