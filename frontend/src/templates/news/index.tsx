import React, { FC } from 'react'
import { graphql, PageProps } from 'gatsby'

import {
  AllStrapiArticlesInterface,
  QueryContextInterface,
  PageInterface,
  SiteInfoInterface,
} from 'interfaces/interfaces'

import { Section, Pagination, Container, Flex } from 'UI'
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs'

import { Layout, NewsList, PageHeader, ContactPanel } from 'components'

interface NewsListInterface extends PageProps {
  data: {
    allStrapiArticles: AllStrapiArticlesInterface;
    strapiNewsPage: PageInterface;
    strapiSiteInfo: SiteInfoInterface;
  }
  pageContext: QueryContextInterface;
}

const NewsListTemplate: FC<NewsListInterface> = ({ data, pageContext, path }) => {
  const { strapiNewsPage, allStrapiArticles, strapiSiteInfo } = data;
  const { edges, totalCount = 0 } = allStrapiArticles;
  const { limit, currentPage } = pageContext;

  return (
    <Layout pageInfo={strapiNewsPage} path={path}>
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/news">Новости</BreadcrumbsItem>
        </Breadcrumbs>
        <Flex space={30} justifyContent="space-between" alignItems="stretch">
          <Section
            title="Новости"
          >
            <NewsList articles={edges} siteInfo={strapiSiteInfo} />
            {(totalCount > limit) && (
              <Pagination
                total={totalCount}
                step={currentPage}
                perPage={limit}
                pageUrl="news"
              />
            )}
          </Section>
          <ContactPanel />
        </Flex>
      </Container>
    </Layout>
  )
}

export default NewsListTemplate;

export const pageQuery = graphql`
  query newsListQuery($skip: Int!, $limit: Int!) {
    allStrapiArticles(
      sort: { fields: [created_at], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          title
          description
          image {
            ...StrapiSingleFileFragment
          }
          slug
          created_at
        }
      }
      totalCount
    }

    strapiNewsPage {
      meta {
        title
        description
      }
    }

    strapiSiteInfo {
      previewImage {
        childImageSharp {
          resize(width: 1920) {
            src
          }
        }
      }
    }
  }
`
