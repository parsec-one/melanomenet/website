import React, { FC } from 'react'
import { graphql, PageProps } from 'gatsby'

import { WikiTagInterface, PageInterface, SiteInfoInterface } from 'interfaces/interfaces'

import { Section, Flex, Container } from 'UI'
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs'

import { Layout, WikiList, PageHeader } from 'components'

interface ArticlePageInterface extends PageProps {
  data: {
    strapiTags: WikiTagInterface;
    strapiWikiPage: PageInterface;
    strapiSiteInfo: SiteInfoInterface;
  }
}

const ArticlePageTemplate: FC<ArticlePageInterface> = ({
  data: { strapiTags, strapiWikiPage, strapiSiteInfo },
  path,
}) => {
  const { wikis = [], name, slug } = strapiTags;

  const pageInfo = {
    meta: {
      title: name,
      description: name,
    },
    background: strapiWikiPage?.background,
  }

  return (
    <Layout
      pageInfo={pageInfo}
      path={path}
    >
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/wiki">О меланоме</BreadcrumbsItem>
          <BreadcrumbsItem to={slug}>{ name }</BreadcrumbsItem>
        </Breadcrumbs>
        <Flex column justifyContent="space-between">
          <Section title={name}>
            <WikiList articles={wikis} siteInfo={strapiSiteInfo} />
          </Section>
        </Flex>
      </Container>
    </Layout>
  )
}

export default ArticlePageTemplate;

export const pageQuery = graphql`
  query wikiTagQuery($slug: String!) {
    ...QueryFields
  }

  fragment QueryFields on Query {
    strapiTags(
      slug: { eq: $slug }
    ) {
      name
      slug
      wikis {
        title
        content
        slug
        description
        created_at
        image {
          ...StrapiSingleFileFragment
        }
      }
    }

    strapiWikiPage {
      background {
        localFile {
          childImageSharp {
            resize(width: 1920) {
              src
            }
          }
        }
        alternativeText
        caption
      }
    }

    strapiSiteInfo {
      previewImage {
        childImageSharp {
          resize(width: 1920) {
            src
          }
        }
      }
    }
  }
`
