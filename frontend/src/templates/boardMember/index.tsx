import React, { FC } from 'react';
import { graphql, PageProps } from 'gatsby';

import { BoardMemberInterface } from 'interfaces/interfaces';

import { Section, HR, Flex, Container } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { Article, Layout, MediaSlider, PageHeader, ContactPanel } from 'components';

interface ArticlePageInterface extends PageProps {
  data: {
    strapiBoardTeams: BoardMemberInterface;
  }
}

const boardMemberTemplate: FC<ArticlePageInterface> = ({
  data: { strapiBoardTeams },
  path,
}) => {
  const {
    name,
    description,
    content = '',
    slug,
    image,
  } = strapiBoardTeams;

  const pageInfo = {
    meta: {
      title: name,
      description,
    },
    previewImageUrl: image?.[0]?.localFile?.childImageSharp?.resolutions?.src,
  }

  return (
    <Layout pageInfo={pageInfo} path={path}>
      <PageHeader />
      <Container>
        <Breadcrumbs>
          <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
          <BreadcrumbsItem to="/about-us/board">Попечительский совет</BreadcrumbsItem>
          <BreadcrumbsItem to={`/${slug}`}>{name}</BreadcrumbsItem>
        </Breadcrumbs>

        <Flex space={30} justifyContent="space-between" alignItems="stretch">
          <Section title={name}>
            <HR />
            {image && <MediaSlider width={240} files={image} />}
            <Article source={content} />
          </Section>
          <ContactPanel />
        </Flex>
      </Container>
    </Layout>
  );
}

export default boardMemberTemplate;

export const pageQuery = graphql`
  query boardMemberQuery($slug: String!) {
    strapiBoardTeams(
      slug: { eq: $slug }
    ) {
      name
      description
      content
      image {
        alternativeText
        caption
        localFile {
          childImageSharp {
            fluid(
              maxWidth: 640
            ) {
              src
              srcSet
              srcSetWebp
            }
            resolutions(width: 1920) {
              src
            }
          }
          publicURL
        }
      }
      slug
      created_at
    }
  }
`
