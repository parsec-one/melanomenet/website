import React, { FC, ImgHTMLAttributes } from 'react';

import { StrapiMediaFileInterface } from 'interfaces/interfaces';

import { Picture, Img } from './styles';

interface ImageInterface extends ImgHTMLAttributes<HTMLImageElement> {
  source?: StrapiMediaFileInterface;
  className?: string;
}

const Image: FC<ImageInterface> = ({
  source = {},
  className,
  ...props
}) => {
  if (!source || !source?.localFile) return (
    <Img
      className={className}
      {...props}
    />
  );

  const { localFile, alternativeText, caption = '' } = source;

  const srcSetWebp = localFile?.childImageSharp?.fluid?.srcSetWebp;
  const srcSet = localFile?.childImageSharp?.fluid?.srcSet;
  const src = localFile?.childImageSharp?.fluid?.src || localFile?.publicURL;

  return (
    <Picture>
      {srcSetWebp && (
        <source
          type="image/webp"
          srcSet={localFile?.childImageSharp?.fluid?.srcSetWebp}
          sizes="
            (max-width: 160px) 160px,
            (max-width: 320px) 640px,
            (max-width: 640px) 640px,
            (max-width: 1280px) 1280px,
            1440px
          "
        />
      )}
      <Img
        src={src}
        srcSet={srcSet}
        alt={alternativeText || caption}
        className={className}
        title={caption || alternativeText}
        sizes="
          (max-width: 160px) 160px,
          (max-width: 320px) 640px,
          (max-width: 640px) 640px,
          (max-width: 1280px) 1280px,
          1440px
        "
        {...props}
      />
    </Picture>
  ) 
}

export default Image;
