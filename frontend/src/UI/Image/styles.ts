import styled from 'styled-components';

export const Picture = styled.picture``;

export const Img = styled.img`
  display: block;
  max-width: 100%;
`;
