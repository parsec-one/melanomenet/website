import styled from 'styled-components'

export const Container = styled.p`
  margin-top: 0;
`;

export const StyledMarkdown = styled.div`
  color: var(--dark-gray);

  h1 {
    font-weight: 600;
    font-size: 32px;
  }

  h1, h2, h3 {
    color: var(--dark-gray);
    margin-top: 1.5em;

    &:nth-child(1) {
      margin-top: 0;
    }
  }
  h1, h2, h3, h4, h5, h6 {
    ::before {
      display: block;
      content: " ";
      margin-top: -100px;
      height: 100px;
      visibility: hidden;
      z-index: -1;
    }
  }

  blockquote {
    background: var(--white);
    margin-left: 0;
    margin-right: 0;
    padding: 1em 1em 1em 2em;
    border-radius: 2px;
    border-left: 4px solid var(--light-gray);

    p {
      margin: 0;
    }
  }

  ul {
    padding-inline-start: 2em;
  }

  img {
    max-width: 100%;
    word-break: break-word;
  }
`
