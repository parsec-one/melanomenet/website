import React, { FC } from 'react';
import ReactMarkdown from 'react-markdown';

import { Container, StyledMarkdown } from './styles';

interface ParagraphInterface {
  max?: number;
  end?: string;
  text: string;
  className?: string;
  isMarkdown?: boolean;
}

const Paragraph: FC<ParagraphInterface> = ({
  max = 0,
  end = '...',
  text = '',
  isMarkdown = false,
  className,
  ...props
}) => {
  const content = (max > 0 && text.length > max)
    ? `${text.substr(0, max)}${end}`
    : text;

  return isMarkdown ? (
    <StyledMarkdown className={className}>
      <ReactMarkdown
        source={content}
        {...props}
      />
    </StyledMarkdown>
  ) : (
    <Container className={className}>
      { content }
    </Container>
  );
}

export default Paragraph;
