import styled from 'styled-components';
import { Link } from 'gatsby';

import { screen } from '../variables'

export const Breadcrumbs = styled.div`
  margin: 20px 0;
  display: flex;

  @media ${screen.tablet} {
    flex-direction: column;

    > :not(:nth-child(1)) {
      margin-top: 5px;
    }
  }
`;

export const BreadcrumbsItem = styled(Link)`
  font-size: 14px;
  color: var(--dark-gray);
  font-weight: bold;
  padding-right: 10px;
  padding-bottom: 20px;

  &:nth-last-child(1) {
    &::after {
      content: none;
    }
  }

  &::after {
    content: '';
    margin-left: 5px;
    display: inline-block;
    width: 6px;
    height: 6px;
    transform:rotate(45deg);
    border-top: 3px solid;
    border-right: 3px solid;
    border-radius: 2px;
    border-color: var(--dark-gray);
  }

  @media ${screen.tablet} {
    padding-bottom: 10px;
  }
`;
