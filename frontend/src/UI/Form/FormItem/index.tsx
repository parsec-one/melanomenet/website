import React from 'react'

import { Container } from './styles'

interface FormItemInterface {
  label: string;
}

const FormItem = ({ label }: FormItemInterface) => (
  <Container>
    <input type="text" name="contact-us" id="contact-us_name"></input>
    <label htmlFor="contact-us_name">Имя и фамилия</label>
  </Container>
)

export default FormItem
