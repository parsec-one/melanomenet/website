import styled from 'styled-components'

import Flex from '../../Flex'

export const Container = styled(Flex).attrs({
  justifyContent: 'stretch',
})`
  position: relative;
  width: 100%;
`

