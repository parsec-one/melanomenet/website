let lastId = 0;

export default (prefix: string = 'id_'): string => {
  lastId++;
  return `${prefix}${lastId}`;
}
