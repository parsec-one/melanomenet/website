import React, { FC } from 'react';
import { SiAdobeacrobatreader } from "react-icons/si";

import { FileContainer, Preview, Name } from './styles';

interface SingleFileInterface {
  name: string;
  caption: string;
  url: string;
  ext: string;
}

const SingleFile: FC<SingleFileInterface> = ({
  name,
  caption,
  url,
  ext,
}) => {
  const getIcon = () => {
    switch(ext) {
      case '.pdf':
        return <SiAdobeacrobatreader />;
      default:
        return ext;
    }
  };

  const fileName = (name.length > 50)
    ? `${name.substr(0, 50)}..${ext}`
    : name;

  return (
    <FileContainer title={caption} href={url}>
      <Preview ext={ext}>
        {getIcon()}
      </Preview>
      <Name>
        {fileName}
      </Name>
    </FileContainer>
  )
}

export default SingleFile;
