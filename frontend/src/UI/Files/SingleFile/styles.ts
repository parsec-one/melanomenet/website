import styled from 'styled-components';

import { screen } from '../../variables';

export const FileContainer = styled.a.attrs({
  target: 'blank',
})`
  width: 240px;
  display: grid;
  grid-auto-flow: row;
  grid-gap: 10px;
  padding: 10px;
  border-radius: 4px;
  color: var(--dark-gray);
  text-align: center;

  :hover {
    background: rgba(69, 177, 234, .1);
  }

  @media ${screen.mobile} {
    grid-auto-flow: column;
    width: 100%;
    text-align: left;
  }
`;

interface FileInterface {
  ext: string;
}

const fileColor = (ext: string): string => {
  switch (ext) {
    case '.pdf':
      return '#e45151'
    default:
      return '#45b1ea'
  }
}

export const Preview = styled.div<FileInterface>`
  display: grid;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 150px;
  justify-self: center;
  color: var(--white);
  border-radius: 1px;
  position: relative;

  font-weight: bold;
  font-size: 24px;

  box-shadow: inset 20px 0 0 rgba(255, 255, 255, .2),
              inset 0 -2px rgba(0, 0, 0, .2);
  background: ${({ ext }) => fileColor(ext)};

  > svg {
    width: 50px;
    height: 50px;
  }

  @media ${screen.mobile} {
    width: 40px;
    height: 60px;
    box-shadow: inset 0 -2px rgba(0, 0, 0, .2);
    clip-path: polygon(0% 0%, calc(100% - 12px) 0%, 100% 12px, 100% 100%, 0% 100%, 0% 50%);

    ::before {
      content: '';
      position: absolute;
      top: 0;
      right: 0;
      width: 12px;
      height: 12px;
      background: var(--black);
      opacity: .15;
    }

    > svg {
      width: 30px;
      height: 30px;
    }
  }
`;

export const Name = styled.span`
  align-self: center;
`;
