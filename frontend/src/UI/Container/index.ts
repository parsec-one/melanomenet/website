import styled from "styled-components"
import { Properties } from "csstype"

import { screen } from '../variables';

export default styled.div<Properties>`
  margin: 0 auto;
  padding: 0 60px;
  width: 100%;
  max-width: ${({ maxWidth }): Properties['maxWidth'] => (maxWidth ? maxWidth : '1400px')};
  position: relative;

  @media ${screen.tablet} {
    padding: 0 30px;
    max-width: 100%;
  }
`
