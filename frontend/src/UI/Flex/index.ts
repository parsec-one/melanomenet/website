import styled, { css } from "styled-components"
import { Properties } from "csstype"

import { screen } from '../variables'

type SpaceType = 5 | 10 | 20 | 30 | 40;
type SizeType = 'mobile' | 'tablet' | 'laptop' | 'desktop'

interface FlexInterface extends Properties {
  space?: SpaceType;
  column?: boolean | SizeType;
  row?: boolean;
}

const spaceContent = (space?: SpaceType, column?: boolean | SizeType) => {
  if (column) {
    if (typeof column === 'string') {
      return css`
        flex-direction: row;
        ${space && `
          > *:not(:nth-child(1)) { margin-top: 0; }
          > *:not(:nth-child(1)) { margin-left: ${space}px; }
        `}

        @media ${screen[column]} {
          flex-direction: column;
          ${space && `
            > *:not(:nth-child(1)) { margin-left: 0; }
            > *:not(:nth-child(1)) { margin-top: ${space}px; }
          `}
        }
      `;
    } else {
      return css`
        flex-direction: column;
        ${space && `
          > *:not(:nth-child(1)) { margin-left: 0; }
          > *:not(:nth-child(1)) { margin-top: ${space}px; }
        `}
      `;
    }
  } else {
    return css`
      flex-direction: row;
      ${space && `
        > *:not(:nth-child(1)) { margin-top: 0; }
        > *:not(:nth-child(1)) { margin-left: ${space}px; }
      `}
    `;
  }
}

export default styled.div<FlexInterface>`
  display: flex;
  align-items: ${({ alignItems = 'normal' }): Properties['alignContent'] => alignItems};
  justify-content: ${({ justifyContent = 'start' }): Properties['justifyContent'] => justifyContent};

  ${({ space, column }: FlexInterface) => spaceContent(space, column)};
`
