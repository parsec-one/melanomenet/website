import React, { ReactNode, ReactNodeArray, HTMLAttributes } from 'react'
import { Properties } from "csstype"

import { Container, PanelContainer, StickyContainer } from './styles'

interface PanelInterface extends HTMLAttributes<HTMLDivElement> {
  children: ReactNode | ReactNodeArray;
  style?: Properties;
  className?: string;
  width?: string;
}

const Panel = ({
  children,
  className,
  width = '340px',
  style,
  ...props
}: PanelInterface) => (
  <PanelContainer width={width} style={style}>
    <StickyContainer>
      <Container
        className={className}
        column
        space={40}
        width={width}
        {...props}
      >
        { children }
      </Container>
    </StickyContainer>
  </PanelContainer>
)

export default Panel;
