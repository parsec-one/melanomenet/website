import styled from 'styled-components'

import { screen } from '../variables'
import Flex from '../Flex'

interface PanelInterface {
  width: string;
}

export const PanelContainer = styled.div`
  min-width: ${({ width }: PanelInterface) => width};
  width: ${({ width }: PanelInterface) => width};
  position: relative;

  @media ${screen.tablet} {
    display: none;
  }
`

export const Container = styled(Flex)`
  width: ${({ width }: PanelInterface) => width};
`;

export const StickyContainer = styled.div`
  top: 0;
  padding: 100px 0 40px;
  position: sticky;
`;
