import styled, { keyframes, css } from 'styled-components'

interface PopupInterface {
  isOpened: boolean;
}

export const Popup = styled.div`
  position: absolute;
  background: var(--white);
  padding: 20px;
  top: calc(100% + 15px);
  right: 0;
  border-radius: 2px;
  box-shadow: 0 5px 20px rgba(0,0,0,.1), inset 0 4px 0 var(--red);
  opacity: 0;
  visibility: hidden;

  &::before {
    content: "";
    width: 8px;
    height: 8px;
    background: var(--red);
    position: absolute;
    display: block;
    right: 20px;
    top: -4px;
    transform: rotate(45deg);
    z-index: -1;
  }

  ${({ isOpened }: PopupInterface) => isOpened && `
    visibility: visible;
    opacity: 1;
  `}
`;

export const Span = styled.span`
  cursor: pointer;
`;

interface ContainerInterface {
  trigger: 'click' | 'hover';
}

export const Container = styled.div`
  position: relative;

  ${({ trigger }: ContainerInterface) => (trigger === 'hover') && `
    ${Popup} {
      transition: opacity .4s cubic-bezier(0.4, 0.0, 0.2, 1), visibility 600ms;
    }

    &:hover {
      > ${Popup} {
        opacity: 1;
        visibility: visible;
      }
    }
  `}
`;
