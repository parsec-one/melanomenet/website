import styled from 'styled-components';

const FullHeight = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
`;

export default FullHeight;
