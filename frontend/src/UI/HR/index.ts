import styled from 'styled-components'

const HR = styled('hr')`
  border-top: 2px solid var(--red);
  width: 100%;
`;

export default HR
