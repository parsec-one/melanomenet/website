import styled from 'styled-components'

interface LinkInterface {
  isActive: boolean;
}

export const Link = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background: none;
  border: ${({ isActive }: LinkInterface) => isActive ? '2px solid' : 'none'};

  ${({ isActive }) => isActive && `
    color: var(--red);
  `}

  &:hover {
    color: var(--red);
  }
`

export const Dot = styled.div`
  width: 5px;
  height: 5px;
  background: #888;
  border-radius: 50%;
`
