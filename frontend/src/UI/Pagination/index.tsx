import React, { Fragment } from 'react'

import Flex from '../Flex'
import { Link, Dot } from './styles'

interface PaginationInterface {
  total: number;
  step: number;
  perPage: number;
  pageUrl: string;
}

const Pagination = ({ total, step, perPage, pageUrl }: PaginationInterface) => {
  const numPages = Math.ceil(total / perPage)

  return (
    <Flex space={10} alignItems="center">
      <Link
        href={`/${pageUrl}`}
        isActive={step === 1}
      >
        { 1 }
      </Link>
      { (numPages > 1) && (
        <Fragment>
          { ( step > 3 ) && (
            <Dot />
          ) }
          { ( step > 1 ) ? (
            Array.from({ length: 3 }).map((none, index) => ((step + index - 1) > 1 && (step + index - 1) <= numPages) ? (
              <Link
                key={`pagination_${step + index - 1}`}
                href={`/${pageUrl}/${step + index - 1}`}
                isActive={step === (step + index - 1)}
              >
                { step + index - 1 }
              </Link>
            ) : null)
          ) : Array.from({ length: 3 }).map((none, index) => ((step + index) > 1 && (step + index) <= numPages) ? (
            <Link
              key={`pagination_${step + index}`}
              href={`/${pageUrl}/${step + index}`}
              isActive={step === (step + index )}
            >
              { step + index }
            </Link>
          ) : null) }
        </Fragment>
      ) }
    </Flex>
  )
}

export default Pagination
