import React, { FC } from 'react';

import { Container, LinkContainer } from './styles';

interface TagInterface {
  link?: string;
  className?: string;
  children: string;
}

const Tag: FC<TagInterface> = ({ link, children, className }) => link ? (
  <LinkContainer to={link} className={className}>
    {children}
  </LinkContainer>
  ) : (
  <Container className={className}>{children}</Container>
);

export default Tag;
