import styled, { css } from 'styled-components';
import { Link } from 'gatsby';

const styles = css`
  font-size: 12px;
  height: 30px;
  padding: 0 8px;
  display: flex;
  border-radius: 15px;
  color: var(--white);
  background-color: var(--dark-gray);
  line-height: 30px;
  font-weight: 600;
  transition: .2s;
`;

export const Container = styled.span`
  ${styles}
`;

export const LinkContainer = styled(Link)`
  ${styles}

  :hover {
    background-color: var(--blue);
    color: var(--white);
  }
`;
