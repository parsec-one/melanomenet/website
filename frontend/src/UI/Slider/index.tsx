import React, { useState, useEffect, ReactNodeArray, FC } from 'react';

import { Container } from './styles';

interface SliderInterface {
  children: ReactNodeArray;
  intervals?: number[] | number;
}

const Slider: FC<SliderInterface> = ({ children = [], intervals = 5000 }) => {
  const [activeSlide, setActiveSlide] = useState(1);

  useEffect(() => {
    const slides = children.length;

    const targetElement = children?.[activeSlide - 1];
    // @ts-ignore: Unreachable code error
    const targetElementId = targetElement?.props?.id;

    const videoElement: HTMLVideoElement | null = (
      // @ts-ignore: Unreachable code error
      targetElement?.type?.target === 'video'
      && targetElementId
      && typeof window !== undefined
    ) ? document.querySelector(`#${targetElementId}`) : null;

    if (videoElement) {
      videoElement.play();
    }

    const interval = Array.isArray(intervals)
      ?
        setTimeout(() => {
          setActiveSlide(activeSlide => {
            if (videoElement) {
              videoElement.pause();
              videoElement.currentTime = 0;
            }

            return (activeSlide < slides) ? activeSlide + 1 : 1;
          })
        }, (intervals[activeSlide - 1]))
      :
        setTimeout(() => {
          setActiveSlide(activeSlide => (activeSlide < slides) ? activeSlide + 1 : 1);
        }, (intervals));

    return () => clearTimeout(interval);
  }, [activeSlide]);

  return (
    <Container activeSlide={activeSlide}>
      { children }
    </Container>
  );
}

export default Slider;
