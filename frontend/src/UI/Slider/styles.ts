import styled, { css } from "styled-components"

const SliderElementStyles = css`
  position: absolute;

  object-fit: cover;
  width: 100%;
  height: 100%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  &::-webkit-media-controls {
     display:none !important;
  }

  z-index: -1;
  opacity: 0;
  transition: opacity 1s;
`;

interface ContainerInterface {
  activeSlide: number;
}

export const Container = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  overflow: hidden;

  > * {
    ${SliderElementStyles}

    :nth-child(${({ activeSlide }: ContainerInterface) => activeSlide}) {
      display: block;
      opacity: 1;
    }
  }
`;
