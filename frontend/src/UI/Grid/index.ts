import styled from 'styled-components';
import { Properties } from 'csstype';

type SpaceType = 10 | 20 | 30 | 40;

interface GridInterface extends Properties {
  space?: SpaceType | Properties['gap'];
}

const Grid = styled.div.attrs((props: Properties) => ({
  style: { ...props },
}))`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(238px, 1fr));
  grid-gap: ${({ space }: GridInterface): Properties['gap'] => space ? `${space}px` : '1em'};
`;

export default Grid;
