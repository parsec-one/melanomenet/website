import styled from "styled-components"

interface ContainerInterface {
  maxWidth: string;
}

export const Div = styled.div``

export const Container = styled.div`
  margin: 0 auto;
  padding: 0 60px;
  width: 100%;
  max-width: ${({ maxWidth }: ContainerInterface): string => (maxWidth ? maxWidth : '1400px')};
  position: relative;
`
