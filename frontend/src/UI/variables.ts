export const size = {
  mobile: '576px',
  tablet: '992px',
  laptop: '1199px',
  desktop: '1200px',
}

export const screen = {
  mobile: `(max-width: ${size.mobile})`,
  tablet: `(max-width: ${size.tablet})`,
  laptop: `(max-width: ${size.laptop})`,
  desktop: `(min-width: ${size.desktop})`,
}
