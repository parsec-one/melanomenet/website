import React, { ReactNode, ReactNodeArray, FC, HTMLAttributes } from 'react'

import { Link } from 'gatsby';

import Container from '../Container'
import Flex from '../Flex'

import { SectionContainer, Title } from './styles'

interface SectionInterface extends HTMLAttributes<HTMLDivElement> {
  title?: string;
  children: ReactNode | ReactNodeArray;
  url?: string;
}

const Section: FC<SectionInterface> = ({ title, children, url = null, ...props }) => (
  <SectionContainer {...props}>
    <Flex column space={30}>
      {(url !== null) ? (
        <Link to={url} title={ title }><Title>{ title }</Title></Link>
      ) : (
        <Title>{ title }</Title>
      )}
      { children }
    </Flex>
  </SectionContainer>
)

export default Section
