import styled from 'styled-components';

export const Title = styled.h1``;

export const SectionContainer = styled.section`
  width: 100%;
  padding: 40px 0;

  &:not(:nth-child(1)) {
    padding-top: 0;
  }

  a {
    ${Title} {
      :hover {
        color: var(--red);
      }
    }
  }
`;
