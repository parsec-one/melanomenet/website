import styled, { keyframes } from 'styled-components'

const Button = styled.button`
  background: var(--red);
  color: var(--white);
  border: 0;
  border-radius: 4px;
  height: 40px;
  line-height: 40px;
  text-transform: uppercase;
  padding: 0 20px;
  width: auto;

  &:hover {
    background: var(--red-darken);
    cursor: pointer;
  }
`

export default Button
