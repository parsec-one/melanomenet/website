import styled, { keyframes } from 'styled-components'

import Flex from '../Flex'

const inputfocus = keyframes`
  from {
    font-size: 0.8em;
    transform: translate(0, 0);
    color: var(--dark-gray);
  }

  to {
    font-size: 0.5em;
    transform: translate(0, -10px);
    color: var(--gray);
    font-weight: bold;
  }
`

export const Container = styled(Flex).attrs({
  justifyContent: 'stretch',
})`
  position: relative;
`

export const LabelComponent = styled.label`
  position: absolute;
  text-transform: uppercase;
  line-height: 40px;
  left: 10px;
  font-size: 0.8em;
  transform: translate(0, 0);
  animation: ${inputfocus} 0.2s ease 0s forwards;
  animation-play-state: paused;
  pointer-events: none;
`

export const InputComponent = styled.input`
  width: 100%;
  border: 0;
  height: 40px;
  padding: 10px 10px 0;

  &:focus {
    ~ ${LabelComponent} {
      animation-play-state: running;
    }
  }
`
