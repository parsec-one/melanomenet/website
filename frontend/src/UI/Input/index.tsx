import React, { HTMLProps } from 'react'

import uniqueId from '../libs/uniqueId'

import { Container, InputComponent, LabelComponent } from './styles'

interface InputInterface extends HTMLProps<HTMLInputElement> {
  label?: string;
}

const Input = ({
  label,
  type = 'text',
  ...props
}: InputInterface) => {
  const id = uniqueId('input');

  return (
    <Container>
      <InputComponent
        { ...props }
        id={id}
        type={type}
      />
      {label && (
        <LabelComponent htmlFor={id}>
          {label}
        </LabelComponent>
      )}
    </Container>
  )
}

export default Input
