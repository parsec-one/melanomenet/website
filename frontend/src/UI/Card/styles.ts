import styled, { css } from 'styled-components';
import { Link } from 'gatsby';

import Flex from '../Flex';

const Styles = css`
  display: flex;
  position: relative;
  align-items: stretch;
  flex-direction: column;
  overflow: hidden;

  color: var(--dark-gray);
  background: var(--white);
  border-radius: 10px;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  transition: .4s cubic-bezier(1, 0.2, 0.0, 0.4);

  &:hover {
    color: var(--text-color);
    box-shadow: 0 15px 30px rgba(0,0,0,.2), 0 3px 0 var(--red);
    transition: .2s cubic-bezier(0.4, 0.0, 0.2, 1);

    h3 {
      color: var(--red);
    }
  }
`;

interface ContainerInterface {
  isLink: boolean;
}

export const Container = styled(Flex)`
  ${Styles}

  ${({ isLink }: ContainerInterface) => isLink && css`
    cursor: pointer;
  `}
`;
