import React, { FC, ReactNode, ReactNodeArray } from 'react';
import { navigate } from 'gatsby';

import { Container } from './styles';

interface CardInterface {
  className?: string;
  to?: string;
  title?: string;
  children: string | ReactNode | ReactNodeArray;
}

const Card: FC<CardInterface> = ({ className, to, title, children }) => {
  const changePage = () => {
    if (!to) return;
    navigate(to);
  }

  return (
    <Container
      className={className}
      title={title}
      isLink={Boolean(to)}
      onClick={() => changePage()}
    >
      { children }
    </Container>
  );
};

export default Card;
