import React from 'react'

import zohoform from './zohoform';

interface ContactFormInterface {
  className?: string;
}

const ContactForm = ({ className }: ContactFormInterface) => (
  <div
    className={className}
    dangerouslySetInnerHTML={{__html: zohoform}}
  />
);

export default ContactForm
