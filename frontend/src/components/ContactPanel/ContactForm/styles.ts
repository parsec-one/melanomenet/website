import styled from 'styled-components'

import { screen } from 'UI/variables'

export const ContactUs = styled.div`
  position: relative;
  padding: 40px;
  border: 2px solid var(--light-gray);
  background: rgba(240, 240, 240, .7);

  @media ${screen.tablet} {
    display: none;
  }
`
