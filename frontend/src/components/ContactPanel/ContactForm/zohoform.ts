import { screen } from 'UI/variables';

export default `
<!-- Note :
- You can modify the font style and form style to suit your website. 
- Code lines with comments Do not remove this code are required for the form to work properly, make sure that you do not remove these lines of code. 
- The Mandatory check script can modified as to suit your business needs. 
- It is important that you test the modified form before going live.-->
<div id='crmWebToEntityForm' class='zcwf_lblLeft crmWebToEntityForm'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0' />
  <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8' />
  <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads3788391000016268023 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory3788391000016268023()' accept-charset='UTF-8'>
    <input type='text' style='display:none;' name='xnQsjsdp' value='a83fc1bc0ed15f6da19eb56eb19633e3476ec1bc81872f3c0751d3b339da326c'></input> 
    <input type='hidden' name='zc_gad' id='zc_gad' value=''></input> 
    <input type='text' style='display:none;' name='xmIwtLD' value='a9170612a624514d08701aa96c99271b3de5cd6290e5487d28526830a5415e51'></input>
    <input type='text'  style='display:none;' name='actionType' value='TGVhZHM='></input>
    <input type='text' style='display:none;' name='returnURL' value='https://melanomenet.ru'></input>
    <!-- Do not remove this code. -->
    <style>
      .crmWebToEntityForm form {
        gap: 10px;
        display: flex;
        flex-wrap: wrap;
        align-items: flex-end;
      }

      @media screen and ${screen.tablet} {
        .crmWebToEntityForm form {
          max-width: 480px;
          margin: 0 auto;
        }
      }

      .crmWebToEntityForm h2 {
        color: var(--black);
      }

      .zcwf_row {
        display: grid;
        gap: 5px;
        flex-grow: 1;
        flex-basis: calc((860px - 100%) * 1000);
      }

      .zcwf_col_fld {
        width: 100%
      }

      .zcwf_help_icon {
        cursor: pointer;
        width: 16px;
        height: 16px;
        display: inline-block;
        background: #fff;
        border: 1px solid #ccc;
        color: #ccc;
        text-align: center;
        font-size: 11px;
        line-height: 16px;
        font-weight: bold;
        border-radius: 50%;
      }

      .wfrm_fld_dpNn {
        display: none;
        opacity: 0;
      }

      .zcwf_col_lab {
        text-transform: uppercase;
        font-size: 13px;
        font-weight: bold;
      }

      .zcwf_label_required:after {
        content: "*";
        color: var(--red);
        margin-left: 4px;
      }

      .crmWebToEntityForm input {
        width: 100%;
        border: 0;
        height: 40px;
        padding: 10px;
      }

      .crmWebToEntityForm input[type='submit'] {
        cursor: pointer;
        text-transform: uppercase;
        color: var(--white);
        background: var(--red);
        border-radius: 4px;
      }

      .crmWebToEntityForm input[type='submit']:hover {
        background: var(--red-darken);
      }
    </style>
    <div class='zcwf_row'>
      <div class='zcwf_col_lab'>
        <label for='Last_Name' class="zcwf_label_required">Имя и фамилия</label>
      </div>
      <div class='zcwf_col_fld'>
        <input type='text' id='Last_Name' name='Last Name' maxlength='80'></input>
      </div>
    </div>
    <div class='zcwf_row'>
      <div class='zcwf_col_lab'>
        <label for='Phone' class="zcwf_label_required">Телефон</label>
      </div>
      <div class='zcwf_col_fld'>
        <input type='text' id='Phone' name='Phone' maxlength='30'></input>
      </div>
    </div>
    <div class='zcwf_row'>
      <div class='zcwf_col_lab'>
        <label for='Email' class="zcwf_label_required">Email</label>
      </div>
      <div class='zcwf_col_fld'>
        <input type='text' ftype='email' id='Email' name='Email' maxlength='100'></input>
      </div>
    </div>
    <div class='zcwf_row wfrm_fld_dpNn'>
      <div class='zcwf_col_lab'>
        <label for='Lead_Source'>Lead Source</label>
      </div>
      <div class='zcwf_col_fld'>
        <select class='zcwf_col_fld_slt' id='Lead_Source' name='Lead Source'  >
          <option selected value='melanomenet.ru'>melanomenet.ru</option>
        </select>
        <div class='zcwf_col_help'>
        </div>
      </div>
    </div>
    <div class='zcwf_row'>
      <div class='zcwf_col_lab'>
      </div>
      <div class='zcwf_col_fld'>
        <input type='submit' id='formsubmit' class='formsubmit zcwf_button' value='Написать' title='Написать' />
      </div>
    </div>
    <script>
    function validateEmail3788391000016268023() {
      var form = document.forms['WebToLeads3788391000016268023'];
      var emailFld = form.querySelectorAll('[ftype=email]');
      var i;
      for (i = 0; i < emailFld.length; i++)
      {
        var emailVal = emailFld[i].value;
        if((emailVal.replace(/^\s+|\s+$/g, '')).length!=0 )
        {
          var atpos=emailVal.indexOf('@');
          var dotpos=emailVal.lastIndexOf('.');
          if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailVal.length)
          {
            alert('Please enter a valid email address. ');
            emailFld[i].focus();
            return false;
          }
        }
      }
      return true;
    }
    
    function checkMandatory3788391000016268023() {
      var mndFileds = new Array('Last Name','Email','Phone');
      var fldLangVal = new Array('Имя','Email','Телефон');
      for(i=0;i<mndFileds.length;i++) {
        var fieldObj=document.forms['WebToLeads3788391000016268023'][mndFileds[i]];
        if(fieldObj) {
          if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
            if(fieldObj.type =='file')
            { 
              alert('Please select a file to upload.'); 
              fieldObj.focus(); 
              return false;
            } 
            alert(fldLangVal[i] +' cannot be empty.'); 
            fieldObj.focus();
            return false;
          }  else if(fieldObj.nodeName=='SELECT') {
            if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
              alert(fldLangVal[i] +' cannot be none.'); 
              fieldObj.focus();
              return false;
            }
          } else if(fieldObj.type =='checkbox'){
            if(fieldObj.checked == false){
              alert('Please accept  '+fldLangVal[i]);
              fieldObj.focus();
              return false;
            } 
          } 
          try {
            if(fieldObj.name == 'Last Name') {
              name = fieldObj.value;
            }
          } catch (e) {}
        }
      }
      if(!validateEmail3788391000016268023()){return false;}
      document.querySelector('.crmWebToEntityForm .formsubmit').setAttribute('disabled', true);
    }
    </script>
    <!-- Do not remove this --- Analytics Tracking code starts -->
    <script id='wf_anal' src='https://crm.zohopublic.com/crm/WebFormAnalyticsServeServlet?rid=a9170612a624514d08701aa96c99271b3de5cd6290e5487d28526830a5415e51gida83fc1bc0ed15f6da19eb56eb19633e3476ec1bc81872f3c0751d3b339da326cgid885e3c1045bd9bdcc91bdf30f82b5696gid14f4ec16431e0686150daa43f3210513'>
    </script>
    <!-- Do not remove this --- Analytics Tracking code ends. -->
  </form>
</div>
`;