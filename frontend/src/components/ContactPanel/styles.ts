import styled from "styled-components"
import { Properties } from "csstype"

import { Panel as UIPanel, Grid } from 'UI'
import { screen } from 'UI/variables'

export const Section = styled.section<Properties>`
  background-image: url('${({ backgroundImage }) => backgroundImage}');
  background-size: cover;
  background-position: center;
  width: 100%;
  height: 100vh;
`

export const Panel = styled(UIPanel)`
  z-index: 1;

  @media ${screen.tablet} {
    display: none;
  }
`;

export const FormContainer = styled(Grid)`
  position: relative;
  padding: 40px;
  background: rgba(240, 240, 240, .7);
  display: grid;
  gap: 20;
  border-radius: 10px;
  backdrop-filter: blur(5px);
  border: 2px solid rgba(16,16,16,.1);

  @media ${screen.tablet} {
    display: none;
  }
`;
