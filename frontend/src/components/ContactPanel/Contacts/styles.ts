import styled from 'styled-components'

export const ContactItem = styled.a`
  color: var(--dark-gray);
  display: flex;

  &:hover {
    text-decoration: none;
  }
`

export const Icon = styled('i')`
  flex: 0 0 20px;
  text-align: center;
  margin-right: 5px;
`
