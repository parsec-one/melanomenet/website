import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { FiPhone, FiMail, FiMapPin } from 'react-icons/fi'

import { ContactsInteface } from 'interfaces/interfaces'

import { Flex } from 'UI'

import { ContactItem, Icon } from './styles'

const Contacts = () => (
  <StaticQuery
    query = {
      graphql`{
        strapiContacts {
          phone
          email
          location {
            address
            link
          }
        }
      }`
    }

    render = {({ strapiContacts }) => {
      const { phone, email, location }: ContactsInteface = strapiContacts;

      return (
        <Flex column space={10}>
          <ContactItem href={`mailto:${email}`}>
            <Icon><FiMail /></Icon>
            <span>{email}</span>
          </ContactItem>
          <ContactItem href={`tel:${phone}`}>
            <Icon><FiPhone /></Icon>
            <span>{phone}</span>
          </ContactItem>
          { Array.isArray(location) && location.map(({ address, link = '#' }, index) => (
            <ContactItem key={`${address}_${index}`} href={link}>
              <Icon><FiMapPin /></Icon>
              <span>{address}</span>
            </ContactItem>
          )) }
        </Flex>
      )
    }}
  />
)

export default Contacts
