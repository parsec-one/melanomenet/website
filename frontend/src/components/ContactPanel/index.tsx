import React, { FC, ReactNode, ReactNodeArray, HTMLAttributes } from 'react';
import { Properties } from 'csstype';

import Contacts from './Contacts';
import ContactForm from './ContactForm';

import { Panel, FormContainer } from './styles';

interface PanelInterface extends HTMLAttributes<HTMLDivElement> {
  style?: Properties;
  className?: string;
  width?: string;
}

const ContactPanel: FC<PanelInterface> = ({ ...props }) => (
  <Panel {...props}>
    <FormContainer>
      <h2>Связаться с нами</h2>
      <ContactForm />
    </FormContainer>
    <Contacts />
  </Panel>
)

export default ContactPanel
