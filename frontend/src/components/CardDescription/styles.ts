import styled from 'styled-components';
import { screen } from 'UI/variables'

export const Container = styled.div`
  display: grid;
  grid-template-rows: min-content auto;
  font-size: 20px;
  max-width: 100%;
  overflow: hidden;

  > h2 {
    font-size: 42px;
    color: var(--dark-gray);

    @media ${screen.tablet} {
      font-size: 2em;
    }

    a {
      color: var(--dark-gray);

      :hover {
        color: var(--blue);
      }
    }
  }
`;
