import React, { FC } from 'react';
import { Link } from 'gatsby';

import { Paragraph } from 'UI';

import { Container } from './styles';

interface CardDescriptionInterface {
  text: string;
  title?: string;
  url?: string;
}

const CardDescription: FC<CardDescriptionInterface> = ({ title, text, url }) => (
  <Container>
    {title && <h2>{ title }</h2>}

    <Paragraph text={text} max={200} end="" isMarkdown={true} />

    {url && <Link to={url}>Подробнее...</Link>}
  </Container>
);

export default CardDescription;
