import React, { FC, Fragment } from 'react';

import { StrapiMenuItemInterface, StrapiMenuPositionInterface } from 'interfaces/interfaces';

import { Flex } from 'UI';

import Dropdown from './Dropdown';
import { Container, Link, SubmenuLink, DropdownLink } from './styles';

type Submenu = Array<{
  name: string;
  slug: string;
}>;

interface SubmenuInterface {
  list: Array<{
    id: number;
    name: string;
    link: string;
    position: StrapiMenuPositionInterface;
  }>;
  path: string;
}

const Submenu: FC<SubmenuInterface> = ({ list }) => (
  <Fragment>
    <Flex column space={10}>
      {list.map(({ name, link, id, position }) => (
        <SubmenuLink
          key={`${name}_${id}`}
          to={link}
          position={position}
        >
          {name}
        </SubmenuLink>
      ))}
    </Flex>
  </Fragment>
)

interface NavigationComponentInterface {
  className?: string;
  path: string;
  menu: StrapiMenuItemInterface[];
}

const Navigation: FC<NavigationComponentInterface> = ({
  className,
  path,
  menu,
}) => (
  <Container className={className}>
    {menu.map(({ name, link = '', id, submenu = [], position }) =>
      (submenu.length > 0) ? (
        <Dropdown
          key={`${name}_${id}`}
          trigger="hover"
          content={Submenu({ list: submenu, path })}
          isTags={false}
        >
          { link ? (
            <DropdownLink
              key={`${name}_${id}`}
              to={link}
              isactive={link === `/${path.split('/')[1]}` ? 1 : 0}
              position={position}
            >
              {name}
            </DropdownLink>
          ) : name}
        </Dropdown>
      ) : (
        <Link
          key={`${name}_${id}`}
          to={link}
          isactive={link === `/${path.split('/')[1]}` ? 1 : 0}
          position={position}
        >
          {name}
        </Link>
      )
    )}
  </Container>
)

export default Navigation;
