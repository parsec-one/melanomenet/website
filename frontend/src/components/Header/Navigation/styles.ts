import styled from 'styled-components';
import { Link as GatsbyLink } from 'gatsby';

import { StrapiMenuPositionInterface } from 'interfaces/interfaces';

import { screen } from 'UI/variables';

export const Container = styled('nav')`
  display: flex;
  position: relative;
  z-index: 1;
  align-items: stretch;

  > * {
    &:not(:nth-child(1)) {
      margin-left: 30px;
    }
  }

  @media ${screen.tablet} {
    flex-direction: column;
    justify-content: start;
    position: fixed;
    z-index: -2;
    padding: 20px 0;
    width: 100vw;
    height: calc(100vh - 78px);
    left: 0;
    top: 0;
    background: var(--white);
    transform: translateY(-100%);
    transition: transform .4s cubic-bezier(0.4, 0.0, 0.2, 1);
    overflow-y: scroll;
    overflow-x: hidden;

    > * {
      &:not(:nth-child(1)) {
        margin-left: 0;
      }
    }
  }
`;

interface LinkInterface {
  isactive?: 1 | 0;
  position: StrapiMenuPositionInterface;
}

export const Link = styled(GatsbyLink)`
  color: ${({ isactive }: LinkInterface) => isactive ? 'var(--red);' : 'var(--text-color)'};
  position: relative;
  display: ${({ position }) => (position?.isHeader ? 'block' : 'none')};
  line-height: 80px;
  white-space: nowrap;
  cursor: pointer;
  transition: .2s;

  &::before {
    content: "";
    display: block;
    position: absolute;
    height: calc(100% + 40px);
    bottom: calc(100% + 140px);
    left: -6px;
    right: -6px;
    background: var(--red);
    z-index: -1;
    border-radius: 4px;
    box-shadow: 0 5px 10px rgba(0,0,0,.1);
    transition: .2s;
  }

  &:hover {
    text-decoration: none;
    color: var(--white);

    &::before {
      bottom: 20px;
    }
  }

  @media ${screen.tablet} {
    max-height: 50px;
    padding: 0 30px;
    line-height: 50px;
    display: ${({ position }) => (position?.isMobile ? 'block' : 'none')};

    ::before {
      content: none;
    }

    :hover {
      color: var(--red);
      ::before {
        content: none;
      }
    }
  }
`;

export const DropdownLink = styled(Link)`
`;

export const SubmenuLink = styled(Link)`
  line-height: 24px;

  ::before {
    content: none;
  }

  :hover {
    color: var(--red);
    ::before {
      content: none;
    }
  }

  @media ${screen.tablet} {
    line-height: 50px;
  }
`;
