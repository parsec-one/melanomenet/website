import React, { Component, ReactNode, ReactNodeArray, MouseEvent } from 'react';

import {
  Container,
  Popup,
  Span,
} from './styles';

interface PropsInterface {
  className?: string;
  children: ReactNode | ReactNodeArray;
  content: ReactNode | ReactNodeArray | string;
  trigger?: 'hover' | 'click';
  isTags?: boolean;
}

interface StateInterface {
  isOpened: boolean;
}

class DropDown extends Component<PropsInterface, StateInterface> {
  state = {
    isOpened: false,
  }

  static defaultProps = {
    trigger: 'click',
    className: '',
  }

  componentDidMount() {
    window.addEventListener('click', this.handleClose);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleClose);
  }

  handleClose = () => {
    this.setState((prevState: StateInterface, props: PropsInterface): StateInterface | null => {
      if (prevState.isOpened) {
        return { isOpened: false };
      } else return null;
    });
  }

  toggleOpen = (event: MouseEvent<any>) => {
    event.stopPropagation();

    this.setState((prevState, props) => {
      return { isOpened: !prevState.isOpened };
    });
  }

  doNothing = (event: MouseEvent<any>) => {
    event.stopPropagation();
  }

  render() {
    const {
      children,
      className,
      content,
      trigger = 'click',
    } = this.props;
    const { isOpened } = this.state;

    return (
      <Container
        onClick={trigger === 'click' ? this.toggleOpen : () => {}}
        trigger={trigger}
        className={className}
      >
        <Span>
          { children }
        </Span>
        <Popup
          onClick={this.doNothing}
          isOpened={isOpened}
        >
          { content }
        </Popup>
      </Container>
    )
  }
}

export default DropDown;
