import styled from 'styled-components';

import { StrapiMenuPositionInterface } from 'interfaces/interfaces';

import { screen } from 'UI/variables';

interface PopupInterface {
  isOpened: boolean;
}

export const Popup = styled.div`
  position: absolute;
  background: var(--white);
  padding: 20px;
  top: calc(100% - 10px);
  right: -6px;
  border-radius: 4px;
  box-shadow: 0 5px 20px rgba(0,0,0,.1), inset 0 4px 0 var(--red);
  opacity: 0;
  visibility: hidden;

  &::before {
    content: "";
    width: 10px;
    height: 5px;
    background: var(--red);
    position: absolute;
    display: block;
    right: 20px;
    top: -4px;
    clip-path: polygon(0% 100%, 50% 0%, 100% 100%);
    z-index: -1;
  }

  ${({ isOpened }: PopupInterface) => isOpened && `
    visibility: visible;
    opacity: 1;
  `}

  @media ${screen.tablet} {
    visibility: visible;
    opacity: 1;
    position: static;
    background: none;
    box-shadow: none;
    padding: 0;
    border-radius: 0;
    margin-left: 30px;
    border-left: 4px solid var(--red);
    color: var(--red);

    &::before {
      content: none;
    }
  }
`;

export const Span = styled.span`
  cursor: pointer;
`;

interface ContainerInterface {
  trigger: 'click' | 'hover';
}

export const Container = styled.div`
  position: relative;

  ${({ trigger }: ContainerInterface) => (trigger === 'hover') && `
    ${Popup} {
      transition: opacity .4s cubic-bezier(0.4, 0.0, 0.2, 1), visibility 600ms;
    }

    &:hover {
      > ${Popup} {
        opacity: 1;
        visibility: visible;
      }
    }
  `}
`;
