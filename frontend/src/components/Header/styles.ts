import styled, { css } from 'styled-components';

import { Flex } from 'UI';
import { screen } from 'UI/variables';

import Navigation from './Navigation';

export const Header = styled('header')`
  position: absolute;
  height: 140px;
  width: 100%;
  display: flex;
  align-items: center;
  z-index: 10;

  &::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    background: var(--white);
    z-index: -1;
    top: -100%;
    transition: .4s;
    box-shadow: 0 0 20px rgba(0, 0, 0, .2);
    
    @supports (backdrop-filter: blur(40px)) {
      background: rgba(255, 255, 255, .5);
      backdrop-filter: blur(40px) saturate(1.2) brightness(1.1);

    }
  }

  &.scrolled {
    height: 80px;
    position: fixed;

    &::before {
      transform: translateY(calc(100% - 2px));
    }
  }

  @media ${screen.tablet} {
    height: 80px;
    position: fixed;

    &::before {
      transition: none;
      transform: translateY(calc(100% - 2px));
    }
  }
`

export const Logo = styled.a`
  display: flex;
  align-items: center;
  color: var(--text-color);
  max-width: 240px;
  height: 80px;
  line-height: 1.2;
`

export const LogoImage = styled.img`
  margin-bottom: 5px;
`

export const BurgerLabel = styled.a`
  display: none;
  position: relative;
  width: 24px;
  height: 24px;
  cursor: pointer;

  @media ${screen.tablet} {
    display: block;
  }
`;

interface MenuInterface {
  isOpen: boolean;
}

export const MenuIcon = styled.div`
  display: none;
  position: relative;
  top: calc(50% - 2px);
  width: 100%;
  height: 2px;

  background: var(--dark-gray);
  transition: .2s cubic-bezier(0.4, 0.0, 0.2, 1);

  &::before,
  &::after {
    content: "";
    display: block;
    position: absolute;
    width: 100%;
    height: 2px;
    background: var(--dark-gray);
    top: calc(100% - 2px);
    transition: .2s;
  }

  &::before {
    transform: rotate(0deg) translateY(-10px);
  }

  &::after {
    transform: rotate(0deg) translateY(10px);
  }

  @media ${screen.tablet} {
    display: block;
  }

  ${({ isOpen }: MenuInterface) => isOpen && css`
    transform: rotate(45deg);

    &::before {
      transform: translateY(0);
    }
    &::after {
      transform: rotate(-90deg);
    }
  `}
`;

export const Nav = styled(Navigation)`
  @media ${screen.tablet} {
    ${({ isOpen }: MenuInterface) => isOpen && css`
      display: block;
      transform: translateY(78px);
    `}
  }
`;

export const NavigationContainer = styled(Flex)`
  height: 80px;
  align-items: stretch;

  @media ${screen.tablet} {
    align-items: center;
  }
`;
