import React, { FC, useState, useEffect } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import { SiteInfoInterface, StrapiMenuInterface } from 'interfaces/interfaces';

import { normalizeMenu } from 'utils/menu';

import { Container, Flex } from 'UI';

import {
  BurgerLabel,
  Header,
  Logo,
  LogoImage,
  MenuIcon,
  Nav,
  NavigationContainer,
} from './styles'

interface HeaderInterface {
  strapiSiteInfo: SiteInfoInterface;
  allStrapiMenus: StrapiMenuInterface;
}

interface HeaderComponentInterface {
  path: string;
}

const HeaderComponent: FC<HeaderComponentInterface> = ({ path = '' }) => {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    isOpen
      ? (document.body.style.overflow = 'hidden')
      : (document.body.style.overflow = 'unset');
  }, [isOpen]);

  const handleOpen = () => {
    setIsOpen(isOpen => !isOpen);
  }

  return (
  <StaticQuery
    query = {
      graphql`{
        strapiSiteInfo {
          logo {
            publicURL
            id
          }
          logoText
        }

        allStrapiMenus(sort: {
          fields: sort
        }) {
          nodes {
            id
            name
            link
            submenu {
              id
              link
              name
              position {
                isFooter
                isHeader
                isMobile
              }
            }
            pages {
              tags {
                id
                name
                slug
              }
            }
            position {
              isFooter
              isHeader
              isMobile
            }
          }
        }
      }`
    }

    render = {({ strapiSiteInfo, allStrapiMenus }: HeaderInterface) => {
      const menuItems = normalizeMenu(allStrapiMenus.nodes);

      return (
        <Header>
          <Container>
            <Flex justifyContent="space-between">
              <Logo href="/">
                <LogoImage src={strapiSiteInfo?.logo?.publicURL} alt='logo' />
              </Logo>
              <NavigationContainer>
                <BurgerLabel onClick={handleOpen}>
                  <MenuIcon isOpen={isOpen} />
                </BurgerLabel>
                <Nav
                  isOpen={isOpen}
                  menu={menuItems}
                  path={path}
                />
              </NavigationContainer>
            </Flex>
          </Container>
        </Header>
      )}
    }
  />
)}

export default HeaderComponent
