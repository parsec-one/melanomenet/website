import React, { FC } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { StaticQuery, graphql } from 'gatsby'

import { TileLayer, Marker, MapContainer, SVGOverlay, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import { FiMapPin, FiPhone, FiMail } from 'react-icons/fi';
import leafet from 'leaflet';

import { ContactsInteface } from 'interfaces/interfaces'

const { divIcon } = leafet;

import Path from './mapPath';
import {
  ContactItem,
  ContactsContainer,
  Wrapper,
  GlobalStyle,
  Icon,
  MetroIcon,
} from './styles';

interface MapInterface {
  strapiContacts: ContactsInteface;
}

const Map: FC = () => {
  const position = {
    lat: 55.801,
    lng: 37.5207,
    zoom: 15,
  };

  const markerPosition = {
    lat: 55.7986,
    lng: 37.5207,
  };

  const metroSokolPosition = {
    lat: 55.8045,
    lng: 37.5165,
  };

  const metroAeroportPosition = {
    lat: 55.8009,
    lng: 37.5302,
  };

  const pathPosition = {
    lat: 55.8045,
    lng: 37.5136,
  };

  const iconMarkup = renderToStaticMarkup(
    <FiMapPin
      style={{
        color: 'var(--red)',
        fill: 'var(--white)',
        width: '48px',
        height: '48px',
      }}
    />
  );

  const iconMetro = renderToStaticMarkup(
    <MetroIcon />
  );

  const customMarkerIcon = divIcon({
    html: iconMarkup,
  });

  const metroIcon = divIcon({
    html: iconMetro,
  });

  const pathHtml = divIcon({
    html: renderToStaticMarkup(<Path
      style={{
        width: '400px',
        height: '250px',
      }}
    />),
  });

  return (
    <StaticQuery
      query = {
        graphql`{
          strapiContacts {
            phone
            email
            location {
              address
              link
            }
          }
        }`
      }

      render = {({ strapiContacts: { phone, email, location } }: MapInterface) => (
        <Wrapper>
          <GlobalStyle />
          <MapContainer
            center={[position.lat, position.lng]}
            zoom={position.zoom}
            scrollWheelZoom={false}
          >
            <ContactsContainer>
              <ContactItem href={`mailto:${email}`}>
                <Icon><FiMail /></Icon>
                <span>{email}</span>
              </ContactItem>
              <ContactItem href={`tel:${phone}`}>
                <Icon><FiPhone /></Icon>
                <span>{phone}</span>
              </ContactItem>
              { Array.isArray(location) && location.map(({ address, link = '#' }, index) => (
                <ContactItem key={`${address}_${index}`} href={link}>
                  <Icon><FiMapPin /></Icon>
                  <span>{address}</span>
                </ContactItem>
              )) }
            </ContactsContainer>
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
              url="	https://a.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"
            />
            <Marker
              position={[markerPosition.lat, markerPosition.lng]}
              icon={customMarkerIcon}
            >
              <Popup>Меланоменет, Чапаевский пер., д. 3</Popup>
            </Marker>
            <Marker
              position={[metroSokolPosition.lat, metroSokolPosition.lng]}
              icon={metroIcon}
            >
              <Popup>Метро Сокол</Popup>
            </Marker>
            <Marker
              position={[metroAeroportPosition.lat, metroAeroportPosition.lng]}
              icon={metroIcon}
            >
              <Popup>Метро Аэропорт</Popup>
            </Marker>
            <SVGOverlay bounds={[
              [pathPosition.lat, pathPosition.lng],
              [pathPosition.lat - 0.0064, pathPosition.lng + 0.018]
            ]}>
              <Path />
            </SVGOverlay>
          </MapContainer>
        </Wrapper>
      )}
    />
  );
}

export default Map;
