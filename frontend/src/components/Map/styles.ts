import styled, { createGlobalStyle, keyframes } from 'styled-components';
import { SiMoscowmetro } from 'react-icons/si';

export const GlobalStyle = createGlobalStyle`
  .leaflet-container {
    width: 100%;
    height: 100%;
    min-height: 500px;
    z-index: 0;
  }

  .leaflet-div-icon {
    background: none;
    border: none;
    margin-left: -24px !important;
    margin-top: -24px !important;
  }

  .leaflet-tile-pane {
    filter: saturate(1.5);
  }
`;

export const Wrapper = styled.div`
  position: relative;
`;

export const ContactsContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  background: var(--white);
  position: absolute;
  z-index: 1000;
  bottom: 20px;
  left: 20px;
  padding: 20px;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  border-radius: 8px;
  max-width: calc(100% - 40px);
`;

export const ContactItem = styled.a`
  color: var(--dark-gray);
  display: flex;

  &:hover {
    text-decoration: none;
  }
`;

export const Icon = styled('i')`
  flex: 0 0 20px;
  text-align: center;
  margin-right: 5px;
`;

export const MetroIcon = styled(SiMoscowmetro)`
  fill: var(--red);
  width: 48px;
  height: 48px;
  stroke: var(--white);
  stroke-linecap: round;
  stroke-width: 4px;
  paint-order: stroke fill markers;

  path {
    transform: scale(0.85) translateY(2px);
  }
`;

const movePath = keyframes`
  from { stroke-dashoffset: 0; }
  to { stroke-dashoffset: -16; }
`;

export const PathGroup = styled('g')`
  fill: none;
  stroke: var(--red);
  stroke-dasharray: 1, 15;
  stroke-linecap: round;
  stroke-width: 6px;
  animation: ${movePath} 1s forwards infinite linear;
`;
