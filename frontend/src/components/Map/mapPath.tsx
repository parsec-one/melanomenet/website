import React, { HTMLAttributes } from 'react';

import { PathGroup } from './styles';

const Path = (props: HTMLAttributes<HTMLOrSVGElement>) => (
  <svg
    width="100%"
    height="100%"
    version="1.1"
    viewBox="0 0 400 250"
    role="img"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <PathGroup>
      <path
        d="m66.8 21.7 70.4 32.4c11 5.08 8.54 22 7.39 29.4-3.56 23.1-16.5 104-16.5 104l24.2 4.84 13.3 12.9"
      />
      <path
        d="m369 163-11.8-6.73c-1.85-1.05-3.09-0.71-3.87 1.1l-11.7 27.6c-0.674 1.58-2.33 2.96-3.93 2.96l-116 0.264-16.1 0.199-1.85 6.49c-1.62 5.68-10.4 5.64-15.8 7.11l-16.1 4.38"
      />
    </PathGroup>
  </svg>
);

export default Path;
