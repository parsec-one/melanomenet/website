import styled, { css, keyframes } from 'styled-components';

import { Grid } from 'UI';
import { screen } from 'UI/variables'

export type PositionType = 'top' | 'active' | 'down' | 'bottom' | false;

interface SliderImageInterface {
  position: PositionType;
  toLeft: boolean;
  isEdge: boolean;
}

interface SliderTextInterface {
  isActive: boolean;
}

export const Container = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 0;

  ::before {
    content: "";
    display: block;
    padding-bottom: 50%;
  }

  @media ${screen.mobile} {
    width: calc(100% - 60px);
    grid-row: 2;

    ::before {
      padding-bottom: calc(100% + 230px);
    }
  }
`;

export const Title = styled.h3`
  font-size: 24px;
  display:  block;
`;

export const IconsContainer = styled.div`
  position: relative;
  display: flex;
`;

export const Icon = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  transform: translateY(-50%);
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: 5;

  :hover {
    border: 3px solid var(--red);
    color: var(--red);
  }

  > svg {
    width: 30px;
    height: 30px;
  }
`;

export const SliderItemContainer = styled(Grid)`
  grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
  position: absolute;
  gap: 20px;
  width: 100%;
  max-width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  overflow: visible;

  @media ${screen.mobile} {
    grid-template-columns: 100%;
    height: auto;
  }
`;

export const ContentContainer = styled(Grid)`
  grid-template-columns: 100%;
  grid-template-rows: min-content auto min-content;
  padding: 20px 20px 20px 0;
  gap: 20px;
  transition: .6s cubic-bezier(0.4, 0.0, 0.2, 1);
  
  ${({ isActive }: SliderTextInterface) => isActive
    ? css`
      opacity: 1;
      visibility: visible;
      z-index: 1;
    `
    : css`
      opacity: 0;
      visibility: none;
      z-index: 0;
    `}
`;

const PositionStyles = {
  top: css`
    transform: translateX(-20%) scale(1.2);
    opacity: 0;
    z-index: 4;
  `,
  active: css`
    transform: translateX(0) scale(1);
    opacity: 1;
    z-index: 3;
  `,
  down: css`
    transform: translateX(20%) scale(0.8);
    opacity: 1;
    z-index: 2;
  `,
  bottom: css`
    transform: translateX(40%) scale(0.6);
    opacity: 1;
    z-index: 1;
  `,
  none: css`
    transform: translateX(60%) scale(0.4);
    opacity: 0;
    z-index: 0;
    visibility: none;
  `
}

const ImageAnimations = {
  top: keyframes`
    from { ${PositionStyles.active} }
    99% { ${PositionStyles.top} }
    to { ${PositionStyles.none} }
  `,

  active: keyframes`
    from { ${PositionStyles.down} }
    to { ${PositionStyles.active} }
  `,

  down: keyframes`
    from { ${PositionStyles.bottom} }
    to { ${PositionStyles.down} }
  `,

  bottom: keyframes`
    from { ${PositionStyles.none} }
    to { ${PositionStyles.bottom} }
  `,
};

export const ImageContainer = styled.div`
  position: relative;
  top: 0;
  left: 0;
  padding: 20px;
  opacity: 1;

  @media ${screen.mobile} {
    grid-row: 1;
  }
`;

export const ImageWrapper = styled.div`
  width: 100%;
  right: 0;
  top: 0;
  height: auto;

  position: relative;
  overflow: hidden;
  border-radius: 10px;
  box-shadow: 0 5px 10px rgba(0, 0, 0, .1);
  transition: .3s cubic-bezier(1,0.2,0.0,0.4);

  :hover {
    box-shadow: 0 15px 30px rgba(0, 0, 0, .1), 0 3px 0 var(--red);
    transition: .2s cubic-bezier(0.4, 0.0, 0.2, 1);
  }

  ::before {
    content: "";
    display: block;
    padding-bottom: 100%;
  }

  > :first-child {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
  }

  ${({ position, isEdge }: SliderImageInterface) => {
    if (isEdge) return PositionStyles.none;
    return position
      ? PositionStyles[position]
      : PositionStyles.none
  }};

  animation-name: ${({ position }: SliderImageInterface) => position
    ? ImageAnimations[position]
    : ''
  };
  animation-direction: ${({ toLeft }: SliderImageInterface) => toLeft ? 'reverse' : 'normal'};
  animation-duration: 0.3s;
  animation-fill-mode: forwards;
`;
