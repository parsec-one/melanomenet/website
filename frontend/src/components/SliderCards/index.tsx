import React, { FC, useState, TouchEvent } from 'react';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { Link } from 'gatsby';

import { StrapiMediaFileInterface } from 'interfaces/interfaces';

import { Image, Paragraph } from 'UI';

import {
  PositionType,
  Container,
  Title,
  Icon,
  SliderItemContainer,
  ImageContainer,
  ImageWrapper,
  ContentContainer,
  IconsContainer,
} from './styles';

interface CardItemInterface {
  name?: string;
  slug?: string;
  description?: string;
  image?: StrapiMediaFileInterface[] | StrapiMediaFileInterface;
}

interface SliderCardsInterface {
  cards: CardItemInterface[]
}

const SliderCards: FC<SliderCardsInterface> = ({ cards }) => {
  const [activeSlide, setSlide] = useState(0);
  const [toLeft, setToLeft] = useState(false);

  const [touchStart, setTouchStart] = useState(0);
  const [touchEnd, setTouchEnd] = useState(0);

  const isLeftSlide = (index: number): boolean => {
    if (activeSlide === 0) return (index === cards.length - 1);
    if (index === activeSlide - 1) return true;
    return false;
  }

  const isRightSlide = (index: number): boolean => {
    if (activeSlide === cards.length - 1) return (index === 0);
    if (index === activeSlide + 1) return true;
    return false;
  }

  const isBottomSlide = (index: number): boolean => {
    if (activeSlide === cards.length - 1) return (index === 0);
    if (index === activeSlide + 1) return true;
    return false;
  }

  const isActiveSlider = (index: number): boolean => {
    return activeSlide === index
  }

  const getPosition = (index: number): PositionType => {
    if (isActiveSlider(index)) return 'active';
    if (isLeftSlide(index)) return 'top';
    if (isRightSlide(index)) return 'down';
    if (isBottomSlide(index)) return 'bottom';
    return false;
  }

  const getIsEdge = (index: number): boolean => {
    if (cards.length < 4) return isBottomSlide(index);
    if (cards.length < 3) return isRightSlide(index);
    if (cards.length < 2) return !isActiveSlider(index);
    return false;
  }

  const changeSlide = (index: number) => {
    setSlide(index);
  }

  const handleLeft = () => {
    setToLeft(true);
    changeSlide(activeSlide === 0 ? cards.length - 1 : activeSlide - 1);
  }

  const handleRight = () => {
    setToLeft(false);
    changeSlide(activeSlide === cards.length - 1 ? 0 : activeSlide + 1);
  }

  function handleTouchStart(event: TouchEvent<HTMLDivElement>) {
    setTouchStart(event.targetTouches[0].clientX);
  }

  function handleTouchMove(event: TouchEvent<HTMLDivElement>) {
    setTouchEnd(event?.targetTouches[0].clientX);
  }

  function handleTouchEnd() {
    if (touchStart - touchEnd > 50) {
      handleRight();
    }

    if (touchStart - touchEnd < -50) {
      handleLeft();
    }
  }

  return (
    <Container
      onTouchStart={(event) => handleTouchStart(event)}
      onTouchMove={(event) => handleTouchMove(event)}
      onTouchEnd={() => handleTouchEnd()}
    >
    {cards.map(({
      name = '',
      slug = '',
      description = '',
      image
    }, index) => getPosition(index) && (
      <SliderItemContainer
        key={`sliderCard_${name}`}
        title={`${name}: ${description}`}
      >
        <ContentContainer
          isActive={isActiveSlider(index)}
        >
          <Title><Link to={slug}>{ name }</Link></Title>

          <Paragraph text={description} max={120} />

          <IconsContainer>
            <Icon style={{ left: '0px' }} onClick={() => handleLeft()}>
              <FiChevronLeft />
            </Icon>
            <Icon style={{ right: '0px' }} onClick={() => handleRight()}>
              <FiChevronRight />
            </Icon>
          </IconsContainer>
        </ContentContainer>

        <ImageContainer>
          <ImageWrapper
            position={getPosition(index)}
            isEdge={getIsEdge(index)}
            toLeft={toLeft}
          >
            <Link to={slug}>
              <Image title={name} source={Array.isArray(image) ? image[0] : image} />
            </Link>
          </ImageWrapper>
        </ImageContainer>
      </SliderItemContainer>
    ))}
    </Container>
  )
}

export default SliderCards;
