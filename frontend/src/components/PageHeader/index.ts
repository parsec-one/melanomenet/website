import styled from 'styled-components'

import { screen } from 'UI/variables'

const PageHeader = styled('div')`
  width: 100%;
  height: 140px;
  position: relative;

  ::before {
    content: '';
    position: absolute;
    top: 198px;
    width: 100%;
    max-width: 1280px;
    height: 2px;
    left: 50%;
    transform: translateX(-50%);
    background: var(--light-gray);
    z-index: -1;

    @media ${screen.tablet} {
      content: none;
    }
  }

  @media ${screen.tablet} {
    height: 80px;

    ::before {
      content: none;
    }
  }
`

export default PageHeader;
