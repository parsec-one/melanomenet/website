import React, { Fragment, ReactNode, ReactNodeArray, FC } from 'react';
import { graphql, StaticQuery, withPrefix } from 'gatsby';
import { Helmet } from 'react-helmet';

import { SiteInfoInterface, PageInterface } from 'interfaces/interfaces';

import Header from './Header';
import Footer from './Footer';

import { GlobalStyle, BackgroundImage } from './styles';
import { Image } from 'UI';

interface PageInfoInterface extends PageInterface {
  previewImageUrl?: string;
}

interface LayoutInterface {
  children: string | ReactNode | ReactNodeArray;
  pageInfo: PageInfoInterface;
  type?: 'article' | 'website' | 'profile' | 'book';
  path: string;
  backgroundColor?: string;
}

interface LayoutQueryInterface {
  strapiSiteInfo: SiteInfoInterface;
}

const Layout: FC<LayoutInterface> = ({
  children,
  path,
  pageInfo: {
    meta,
    previewImageUrl = '',
    background,
  },
  type = 'website',
  backgroundColor,
}) => (
  <StaticQuery
    query = {
      graphql`{
        strapiSiteInfo {
          title
          previewImage {
            childImageSharp {
              resize(width: 1920) {
                src
              }
            }
          }
          favicon {
            childImageSharp {
              fluid(maxWidth: 180, srcSetBreakpoints: [16, 32, 180, 192]) {
                srcSet
              }
            }
          }
          analytics {
            googleTagId
            googleAnalyticsId
          }
          site_url
        }
      }`
    }

    render={({ strapiSiteInfo }: LayoutQueryInterface) => {
      const { analytics, favicon, previewImage, site_url, title } = strapiSiteInfo;
      const faviconSizes = favicon?.childImageSharp?.fluid?.srcSet || '';
      const faviconUrls = faviconSizes.replace(/\s+[0-9]+(\.[0-9]+)?[wx]/g, '').split(/,/);

      const defaultImageUrl = previewImage?.childImageSharp?.resize?.src || '';

      const url = site_url?.replace(/\/$/, '');

      const backgroundImageFile = Array.isArray(background)
        ? background[0]
        : background;

      return (
        <Fragment>
          <Helmet>
            <meta charSet="utf-8" />
            <title>{ meta.title } | { title }</title>
            <meta name="viewport" content="width=300, initial-scale=1" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta name="description" content={meta.description} />
            <meta name="title" content={meta.title} />
            <meta property="og:locale" content="ru_RU" />
            <meta property="og:image" content={`${url}${previewImageUrl || defaultImageUrl}`} />
            <meta property="og:site_name" content={meta.title} />
            <meta property="og:description" content={meta.description} />
            <meta property="og:url" content={path} />
            <meta name="theme-color" content="#cc2d29" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
            <meta name="apple-mobile-web-app-status-bar-style" content="#cc2d29" />

            {type === 'article' ? (
              <meta property='og:type' content={type} />
            ) : (
              <meta property='og:type' content="website" />
            )}
            <link
              rel="apple-touch-icon"
              sizes="180x180"
              href={faviconUrls[2]}
            />
            <link
              rel="shortcut icon"
              type="image/png"
              sizes="16x16"
              href={faviconUrls[0]}
            />
            <link
              rel="icon"
              type="image/png"
              sizes="32x32"
              href={faviconUrls[1]}
            />
            <link
              rel="icon"
              type="image/png"
              sizes="192x192"
              href={faviconUrls[3]}
            />
          </Helmet>
          { backgroundImageFile && (
            <BackgroundImage>
              <Image source={backgroundImageFile} />
            </BackgroundImage>
          )}
          <Header path={path} />
          { children }
          <Footer />
          <Helmet>
            <script defer src={withPrefix('scripts.js')} />
            <script defer src={withPrefix('utils/fixedScroll.js')} />
          </Helmet>
          <GlobalStyle />
        </Fragment>
      )}
    }
  />
)

export default Layout
