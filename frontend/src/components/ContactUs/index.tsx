import React, { FC } from 'react';

import { Section, Container } from 'UI';

import ContactForm from 'components/ContactPanel/ContactForm';

import { StyledContainer } from './styles';

const ContactUs: FC = () => (
  <StyledContainer id="contact_us-form">
    <Container>
      <Section title="Написать нам">
        <ContactForm />
      </Section>
    </Container>
  </StyledContainer>
);

export default ContactUs
