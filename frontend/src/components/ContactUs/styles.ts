import styled from 'styled-components';

export const StyledContainer = styled.div`
  background: var(--red);

  h1 {
    color: var(--white);
  }

  color: var(--white);

  .crmWebToEntityForm input {
    background: rgba(255, 255, 255, .7);
  }

  .crmWebToEntityForm input[type='submit'] {
    border: 2px solid;
  }

  ::before {
    display: block;
    content: " ";
    visibility: hidden;
    z-index: -1;
  }

  input {
    :focus {
      background: var(--white);
      box-shadow: 0 10px 20px rgba(0,0,0,.2);
    }
  }
`;
