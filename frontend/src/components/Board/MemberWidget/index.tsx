import React, { FC } from 'react';
import { StaticQuery, graphql, Link } from 'gatsby'
import { FiFeather } from 'react-icons/fi';

import { BoardMemberInterface } from 'interfaces/interfaces';

import { Flex, Container as ContainerUI, Paragraph } from 'UI';

import { Member, Image, Content, ImageWrapper, Container } from './styles';

const Widget: FC<BoardMemberInterface> = () => (
  <StaticQuery
    query = {
      graphql`{
        strapiFoundCreatorWidget {
          member {
            name
            slug
          }
          image {
            ...StrapiSingleFileFragment
          }
          content
        }
      }`
    }

    render = {({ strapiFoundCreatorWidget }) => {
      const { content, image, member } = strapiFoundCreatorWidget;
      const { name, slug } = member;

      return (
        <Container>
          <ContainerUI>
            <Flex>
              <Content space={20}>
                <FiFeather
                  size={40}
                  style={{
                    minWidth: '40px',
                    color: 'var(--red)',
                    marginTop: '20px',
                  }}
                />

                <Flex column space={20}>
                  <Paragraph text={content} isMarkdown={true} />

                  <Member column>
                    <small>Президент фонда</small>

                    <h3><Link to={slug}>{name}</Link></h3>
                  </Member>
                </Flex>
              </Content>

              {image && (
                <ImageWrapper>
                  <Image source={image} />
                </ImageWrapper>
              )}
            </Flex>
          </ContainerUI>
        </Container>
      );
    }}
  />
);

export default Widget;
