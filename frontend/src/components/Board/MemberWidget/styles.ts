import styled from 'styled-components';

import { Image as ImageUI, Flex } from 'UI';
import { screen } from 'UI/variables';

export const Container = styled.div`
  background: var(--white);
  padding: 40px 0;
  position: relative;
  border-width: 4px 0 4px 0;
  border-style: solid;
  border-color: var(--red);
  overflow: hidden;
  max-width: 100vw;

  ::before {
    background: repeating-linear-gradient(0deg, var(--red), var(--red) 10px, var(--white) 10px, var(--white) 20px);
    content: '';
    opacity: .2;
    width: 100px;
    height: 50px;
    transform: rotate(-45deg);
    top: -10px;
    left: -30px;
    position: absolute;
    pointer-events: none;
    z-index: 1;
    mix-blend-mode: multiply;
  }
`;

export const Content = styled(Flex)`
  width: 100%;
  min-width: 66%;

  @media ${screen.tablet} {
    min-width: 100%;
  }
`;

export const Member = styled(Flex)`
`;

export const ImageWrapper = styled.div`
  position: relative;
  overflow: hidden;
  height: auto;
  width: 100%;

  ::before {
    content: "";
    display: block;
    padding-bottom: 100%;
  }
`;

export const Image = styled(ImageUI)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;

  @media ${screen.tablet} {
    display: none;
  }
`;
