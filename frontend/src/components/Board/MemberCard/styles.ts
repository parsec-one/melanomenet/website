import styled from 'styled-components';

import { Flex, Image, Card as CardUI } from 'UI';

export const Card = styled(CardUI)`
  max-width: 430px;
`;

export const Container = styled(Flex)`
  padding: 20px;
  height: 100%;
`;

export const Photo = styled(Image)`
  position: absolute;
  min-height: 100%;
  object-fit: cover;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;

export const PhotoContiner = styled.div`
  width: 100%;
  position: relative;
  padding-top: 100%;
  overflow: hidden;
`;
