import React, { FC } from 'react';
import { Link } from 'gatsby';

import { BoardMemberInterface } from 'interfaces/interfaces';

import { Paragraph } from 'UI';

import { Card, Container, Photo, PhotoContiner } from './styles';

const MemberCard: FC<BoardMemberInterface> = ({
  name,
  slug,
  image,
  description = '',
}) =>{ 
  return (
  <Card title={description} to={`/${slug}`}>
    <PhotoContiner>
      <Photo source={image?.[0]} />
    </PhotoContiner>

    <Container column space={10}>
      <h3><Link to={`/${slug}`}>{name}</Link></h3>
      <Paragraph max={140} text={description} />
    </Container>
  </Card>
);}

export default MemberCard;
