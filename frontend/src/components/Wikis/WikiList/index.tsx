import React, { FC } from 'react';

import { WikiItemInterface, SiteInfoInterface } from 'interfaces/interfaces';
import { Flex, Grid } from 'UI';

import { Container } from './styles';
import WikiItem from '../WikiItem';

interface WikisListInteface {
  articles: Array<WikiItemInterface>;
  siteInfo: SiteInfoInterface;
}

const NewsList: FC<WikisListInteface> = ({ articles, siteInfo }) => (
  <Flex column space={40}>
    <Container space={30}>
      { (articles && articles.length > 0) && articles.map((item, index) => (
        <WikiItem key={`newsItem_${index}`} {...item} siteInfo={siteInfo} />
      )) }
    </Container>
  </Flex>
)

export default NewsList
