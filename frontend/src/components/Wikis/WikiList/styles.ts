import styled from 'styled-components';

import { Grid } from 'UI';
import { screen } from 'UI/variables';

export const Container = styled(Grid)`
  grid-auto-rows: 1fr;

  @media ${screen.mobile} {
    grid-auto-rows: auto;
  }
`;
