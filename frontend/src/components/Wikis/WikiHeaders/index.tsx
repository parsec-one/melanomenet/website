import React, { FC } from 'react';

import { Flex } from 'UI';

import { HeaderLink, Icon } from './styles';

interface WikiHeaderslinterface {
  content?: string;
  path: string;
}

const WikiHeaders: FC<WikiHeaderslinterface> = ({
  content = '',
  path,
  ...props
}) => {
  const headers = content.match(/^[#]+.*?(&|$)/gm)?.map(( item: string ) => ({
    text: item.replace(/^[#|\ ]+.*?/g, ''),
    level: item.match(/[#].*?/g)?.length,
  }));

  return (
    <Flex column space={10}>
      {headers?.map(({ text, level }, index) => (
        <HeaderLink
          key={`${path}${text}_${index}`}
          href={`${path}#${text}`}
          title={text}
        >
          { (level && (level > 1)) && Array.from(Array(level - 1)).map((none, index) => (
            <Icon
              size="14px"
              key={`icon_${text}_${index}`}
            />
          )) }
          { text }
        </HeaderLink>
      ))}
    </Flex>
  )
}

export default WikiHeaders;
