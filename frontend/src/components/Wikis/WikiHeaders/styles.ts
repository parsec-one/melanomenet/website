import styled from "styled-components"
import { FiChevronRight } from 'react-icons/fi';

export const Icon = styled(FiChevronRight)`
  flex-basis: 20px;
  min-width: 20px;
  margin-top: 3px;
`;

export const HeaderLink = styled.a`
  color: var(--dark-gray);
  line-height: 1.2;
  display: flex;
  font-size: 14px;
`;
