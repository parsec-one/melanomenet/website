import styled from 'styled-components';

import { FiCalendar } from 'react-icons/fi';

import { Flex } from 'UI'
import { screen } from 'UI/variables'

export const WikiContainer = styled(Flex)`
  padding: 20px;
  height: 100%;
`

export const Title = styled.h3``;

export const ImagePreview = styled.div`
  height: 200px;
  min-height: 200px;
  width: 100%;
  min-width: 100%;
  background-color: var(--gray);
  background-size: cover;
  background-position: center;

  @media ${screen.mobile} {
    height: 150px;
    min-height: 150px;
    width: 100%;
    min-width: 100%;
  }
`;

export const DateContainer = styled.span`
  margin-top: auto !important;
  font-size: 0.8em;
`

export const DateIcon = styled(FiCalendar)`
  width: 20px;
  text-align: center;
  margin-right: 5px;
`

export const TagsContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: flex-end;
`;
