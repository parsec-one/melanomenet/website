import React, { FC } from 'react';

import { WikiItemInterface, SiteInfoInterface } from 'interfaces/interfaces';
import { Paragraph, Card, Tag } from 'UI';

import { ImagePreview, WikiContainer, Title, TagsContainer } from './styles';

interface WikiItemComponent extends WikiItemInterface {
  siteInfo: SiteInfoInterface;
}

const WikiItem: FC<WikiItemComponent> = ({
  title,
  slug,
  description,
  image,
  siteInfo,
  tags,
}) => {
  const src = image?.[0]?.localFile?.childImageSharp?.fluid?.src;
  const defaultImageSrc = siteInfo?.previewImage?.childImageSharp?.resize?.src || '';

  return (
    <Card title={title} to={`/wiki/${slug}`}>
      <ImagePreview style={{ backgroundImage: `url(${ src || defaultImageSrc })` }} />
      <WikiContainer column space={10}>
        <Title>{ title }</Title>

        <Paragraph max={140} text={description} end="..." />

        {tags && (
          <TagsContainer>
            {tags.map(({ name, slug }) => (
              <Tag key={`wikiTag_${slug}`} link={slug}>{ name }</Tag>
            ))}
          </TagsContainer>
        )}
      </WikiContainer>
    </Card>
  )
}

export default WikiItem
