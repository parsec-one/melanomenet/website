import React, { FC, createElement } from 'react';

import { Paragraph } from 'UI';

interface ArticleInterface {
  source: string;
}

const Article: FC<ArticleInterface> = ({ source }) => {
  const HeadingRenderer = ({ level, children }: any) => {
    const slug = `${children[0]?.props?.value}`;
    return createElement(`h${level}`, { id: slug }, children);
  }

  return (
    <Paragraph
      isMarkdown
      text={source}
      renderers={{ heading: HeadingRenderer }}
    />
  )
}

export default Article
