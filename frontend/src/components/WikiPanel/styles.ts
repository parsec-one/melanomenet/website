import styled from 'styled-components';

import { Panel as UIPanel, Flex, Card } from 'UI';
import { screen } from 'UI/variables';

export const Panel = styled(UIPanel)`
  z-index: 1;

  @media ${screen.tablet} {
    display: none;
  }
`;

export const Title = styled.h3``;

export const HeadersContainer = styled(Flex)`
  border-radius: 5px;
  padding: 20px;
  background: var(--white);
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
`;
