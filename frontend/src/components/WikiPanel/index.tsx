import React, { FC } from 'react';

import { WikiHeaders } from '../Wikis';
import { Panel, HeadersContainer, Title } from './styles';

interface WikiPanelinterface {
  content?: string;
  path: string;
}

const WikiPanel: FC<WikiPanelinterface> = ({
  content = '',
  path,
  ...props
}) => (
  <Panel {...props}>
    <HeadersContainer column space={20} {...props}>
      <Title>Содержание:</Title>
      <WikiHeaders content={content} path={path} />
    </HeadersContainer>
  </Panel>
)

export default WikiPanel;
