import styled from 'styled-components';
import { Link as GatsbyLink } from 'gatsby';

import { StrapiMenuPositionInterface } from 'interfaces/interfaces';

export const Footer = styled('footer')`
  padding: 60px 0;
  width: 100%;
  display: flex;
  align-items: center;
  background: var(--almost-white);
  font-size: 14px;
`

export const Logo = styled.a`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  color: var(--text-color);
  height: 24px;
  line-height: 1.2;
`

export const LogoImage = styled.img`
  margin-bottom: 5px;
`

interface LinkInterface {
  position: StrapiMenuPositionInterface;
}

export const Link = styled(GatsbyLink)`
  color: var(--text-color);
  position: relative;
  display: block;
  height: 100%;
  display: ${({ position }: LinkInterface) => (position?.isFooter ? 'block' : 'none')};

  &:hover {
    text-decoration: none;
    color: var(--red);
  }  
`

export const SubmenuLink = styled(Link)`
  display: grid;
  grid-template-columns: 20px auto;
  
  > svg {
    align-self: center;
    justify-self: center;
  }
`
