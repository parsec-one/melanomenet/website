import React, { FC } from 'react'

import { FiChevronRight } from 'react-icons/fi';

import { StrapiMenuItemInterface } from 'interfaces/interfaces'

import { SubmenuLink } from './styles'

const SubMenu: FC<StrapiMenuItemInterface> = ({ name, link, position }) => (
  <SubmenuLink
    position={position}
    to={link}
  >
    <FiChevronRight />
    {name}
  </SubmenuLink>
)

export default SubMenu;
