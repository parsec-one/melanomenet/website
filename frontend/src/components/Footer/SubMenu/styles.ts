import styled from 'styled-components';

import { Link } from '../styles';

export const SubmenuLink = styled(Link)`
  display: grid;
  grid-template-columns: 20px auto;
  display: ${({ position }) => (position?.isFooter ? 'block' : 'none')};

  > svg {
    align-self: center;
    justify-self: center;
  }
`
