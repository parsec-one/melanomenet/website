import React, { FC, Fragment } from 'react'
import { StaticQuery, graphql } from 'gatsby'

import { FiChevronRight } from 'react-icons/fi';

import { SiteInfoInterface, StrapiMenuInterface } from 'interfaces/interfaces'
import { Flex, Container } from 'UI'
import Contacts from '../ContactPanel/Contacts'

import SubMenu from './SubMenu';
import { Footer, Logo, LogoImage, Link } from './styles'

interface FooterInterface {
  strapiSiteInfo: SiteInfoInterface;
  allStrapiMenus: StrapiMenuInterface;
}

const FooterComponent: FC = () => {
  return (
    <StaticQuery
      query = {
        graphql`{
          strapiSiteInfo {
            logo {
              publicURL
              id
            }
            logoText
          }

          allStrapiMenus {
            nodes {
              id
              name
              link
              submenu {
                id
                link
                name
                position {
                  isFooter
                  isHeader
                  isMobile
                }
              }
              position {
                isFooter
                isHeader
                isMobile
              }
            }
          }
        }`
      }

      render = {({ strapiSiteInfo, allStrapiMenus }: FooterInterface) => (
        <Footer>
          <Container>
            <Flex
              column="tablet"
              space={40}
              justifyContent="space-between"
            >
              <Logo href="/">
                <LogoImage src={strapiSiteInfo.logo.publicURL} alt='logo' />
                <small>{strapiSiteInfo.logoText}</small>
              </Logo>
              <Flex
                column="mobile"
                space={40}
                justifyContent="space-between"
              >
                <nav>
                  <Flex column space={10}>
                    {allStrapiMenus.nodes.map(({ name, link, id, submenu = [], position }) => (
                      <Fragment key={`footer_Items_${id}`}>
                        <Link
                          key={`footer_footerItems_${id}`}
                          to={link}
                          position={position}
                        >
                          {name}
                        </Link>

                        {submenu.map((submenuItem) => (
                          <SubMenu
                            key={`footer_submenu_${submenuItem.id}`}
                            {...submenuItem}
                          />
                        ))}
                      </Fragment>
                    ))}
                  </Flex>
                </nav>
                <Contacts />
              </Flex>
            </Flex>
          </Container>
        </Footer>
      )}
    />
  )
}

export default FooterComponent
