import styled, { createGlobalStyle } from 'styled-components'
import { screen } from 'UI/variables'

export const GlobalStyle = createGlobalStyle`
  html {
    --color: 0, 100%; /*the base color*/
    --l: 50%; /*the initial lightness*/

    --white: #fff;
    --almost-white: #f7f7f7;
    --light-gray: #ccc;
    --gray: #888;
    --dark-gray: #444;
    --almost-black: #222;
    --black: #000;

    --red-lighten: #e93b38;
    --red: #cc2d29;
    --red-darken: #a12220;

    --light-blue: #8af;
    --blue: #48f;
    --dark-blue: #248;

    --text-color: #222;
    --link-color: #48f;
  }

  html, * {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    font-family: "Open Sans", sans-serif;
    background: var(--white);
  }

  h2, h3, h4, h5, h6 {
    font-weight: 600;
    margin: 0;
  }

  h1 {
    font-family: "Open Sans", sans-serif;
    margin: 0;
    font-size: 3em;
    font-weight: 500;
    color: var(--dark-gray);
    line-height: 1.0;

    @media ${screen.tablet} {
      font-size: 2em;
    }
  }

  h2 {
    font-size: 24px;
    font-weight: 600;
  }

  h3 {
    font-weight: 700;
    font-size: 18px;
  }

  a {
    text-decoration: none;
    color: var(--link-color);
  }

  a:hover {
    color: var(--dark-blue);
    cursor: pointer;
  }

  form {
    margin: 0;
  }

  input {
    outline: none;
    border-radius: 2px;
    box-shadow: 0 2px 5px rgba(0,0,0,.1);
    transition: .2s cubic-bezier(0.4, 0.0, 0.2, 1);

    :hover,
    :active,
    :focus {
      color: var(--text-color);
      box-shadow: 0 10px 20px rgba(0,0,0,.2);
    }

    :focus {
      color: var(--text-color);
      box-shadow: 0 10px 20px rgba(0,0,0,.2), inset 0 -2px 0 var(--red);
    }
  }
`;

export const BackgroundImage = styled.div`
  position: absolute;
  overflow: hidden;
  width: 100%;
  height: 200px;
  display: flex;
  align-items: center;

  > picture {
    width: 100%;

    img {
      width: 100%;
    }
  }

  ::before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: var(--white);
    opacity: .6;
  }
`;
