import styled from 'styled-components'

export const StyledArticle = styled.div`
  color: var(--dark-gray);

  > h1 {
    font-weight: 600;
    font-size: 32px;
  }

  > h1, h2, h3 {
    color: var(--dark-gray);
    margin-top: 1.5em;

    &:nth-child(1) {
      margin-top: 0;
    }
  }
`;
