import React, { FC } from 'react';

import { Paragraph } from 'UI';

interface ArticleInterface {
  source: string;
}

const Article: FC<ArticleInterface> = ({ source }) => (
  <Paragraph isMarkdown text={source} />
)

export default Article
