import styled from 'styled-components';

import { FiCalendar } from 'react-icons/fi';

import { Flex } from 'UI'
import { screen } from 'UI/variables'

export const NewsContainer = styled(Flex)`
  padding: 20px;
  height: 100%;
`

export const Title = styled.h3`
`;

export const TitleImage = styled.div`
  height: 200px;
  min-height: 200px;
  width: 100%;
  min-width: 100%;
  padding: 20px;
  background-size: cover;

  @media ${screen.mobile} {
    height: 150px;
    min-height: 150px;
    width: 100%;
    min-width: 100%;
  }
`;

export const DateContainer = styled.span`
  padding-top: 10px;
  margin-top: auto !important;
  font-size: 0.8em;
`

export const DateIcon = styled(FiCalendar)`
  width: 20px;
  text-align: center;
  margin-right: 5px;
`
