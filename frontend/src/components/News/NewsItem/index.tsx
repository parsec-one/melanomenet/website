import React, { FC } from 'react';
import { Link } from 'gatsby';

import { NewsItemInterface, SiteInfoInterface } from 'interfaces/interfaces';
import { Card } from 'UI';

import { DateContainer, DateIcon, TitleImage, NewsContainer, Title } from './styles'

interface NewsItemComponent extends NewsItemInterface {
  siteInfo: SiteInfoInterface;
}

const NewsItem: FC<NewsItemComponent> = ({
  title = '',
  slug = '',
  description,
  created_at: createdAt,
  image = [],
  siteInfo,
}) => {
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  const dateTime = new Date(createdAt);
  const dateText = dateTime.toLocaleDateString('ru-RU', dateOptions)

  const src = image?.[0]?.localFile?.childImageSharp?.fluid?.src;
  const defaultImageSrc = siteInfo?.previewImage?.childImageSharp?.resize?.src || '';

  return (
    <Card title={description} to={`/news/${slug}`}>
      <TitleImage style={{ backgroundImage: `url(${ src || defaultImageSrc })` }} />
      <NewsContainer column space={10}>
        <Title>{ title }</Title>
        <DateContainer title={dateText}>
          <DateIcon />
          { dateText }
        </DateContainer>
      </NewsContainer>
    </Card>
  )
}

export default NewsItem
