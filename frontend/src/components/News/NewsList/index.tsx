import React, { FC } from 'react';

import { StrapiNodeInerface, SiteInfoInterface } from 'interfaces/interfaces';
import { Flex, Grid } from 'UI';

import { Container } from './styles';
import NewsItem from '../NewsItem';

interface NewsListInteface {
  articles: StrapiNodeInerface[];
  siteInfo: SiteInfoInterface;
}

const NewsList: FC<NewsListInteface> = ({ articles, siteInfo }) => (
  <Flex column space={40}>
    <Container space={30}>
      { (articles && articles.length > 0) && articles.map(({ node }, index) => (
        <NewsItem key={`newsItem_${index}`} {...node} siteInfo={siteInfo} />
      )) }
    </Container>
  </Flex>
)

export default NewsList
