import React, { FC } from 'react';
import { graphql, StaticQuery } from 'gatsby';

import { AllStrapiArticlesInterface, SiteInfoInterface } from 'interfaces/interfaces';
import { getDateText, getDateDescription } from 'utils/date';

import { Image, Flex } from 'UI';

import { Panel, Container, Title, ArticleItem, Date, Link } from './styles'

interface LastNewsInterface {
  allStrapiArticles: AllStrapiArticlesInterface;
  strapiSiteInfo: SiteInfoInterface;
}

const LastNewsPanel: FC = ({ ...props }) => (
  <Panel {...props}>
    <StaticQuery
      query = {
        graphql`{
          strapiSiteInfo {
            previewImage {
              childImageSharp {
                fluid(
                  cropFocus: CENTER,
                  maxWidth: 100,
                  jpegProgressive: true,
                  fit: COVER,
                  maxHeight: 100
                ) {
                  src
                }
              }
            }
          }

          allStrapiArticles(
            sort: { fields: [created_at], order: DESC }
            limit: 4
            skip: 0
          ) {
            edges {
              node {
                title
                description
                slug
                created_at
                image {
                  localFile {
                    childImageSharp {
                      fluid(
                        cropFocus: CENTER,
                        maxWidth: 100,
                        jpegProgressive: true,
                        fit: COVER,
                        maxHeight: 100
                      ) {
                        src
                      }
                    }
                  }
                }
              }
            }
            totalCount
          }
        }`
      }

      render={({ allStrapiArticles, strapiSiteInfo }: LastNewsInterface) => {
        const previewImageSrc = strapiSiteInfo?.previewImage?.childImageSharp?.fluid?.src;

        return (
          <Container column space={20}>
            <Title>Новости:</Title>
            <Flex column space={20}>
              {allStrapiArticles.edges.map(({ node: { title, slug, description, image, created_at }}) => (title && slug) && (
                <Link key={`newsPanel_${slug}`} title={description} to={`/news/${slug}`}>
                  <ArticleItem space={10} gridRow={1} gridTemplateColumns="70px auto">
                    {(image && image.length > 0) ? (
                      <Image source={image[0]} title={title} />
                    ) : (
                      <Image src={previewImageSrc} title={title} />
                    )}
                    <span>
                      {title}
                    </span>
                    <Date title={getDateDescription(created_at)}>
                      {getDateText(created_at)}
                    </Date>
                  </ArticleItem>
                </Link>
              ))}
            </Flex>
          </Container>
        )}
      }
    />
  </Panel>
)

export default LastNewsPanel;
