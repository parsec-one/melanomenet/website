import styled from "styled-components";
import { Link as GatsbyLink } from 'gatsby';

import { Panel as UIPanel, Flex, Grid } from 'UI'
import { screen } from 'UI/variables'

export const Panel = styled(UIPanel)`
  z-index: 1;

  @media ${screen.tablet} {
    display: none;
  }
`;

export const Title = styled.h3``;

export const Container = styled(Flex)`
  border-radius: 5px;
  padding: 20px;
  background: var(--white);
  box-shadow: 0 5px 30px rgba(0,0,0,.1);
`;

export const ArticleItem = styled(Grid)`
  line-height: 1.2;
`;

export const Date = styled.small`
  color: var(--dark-gray);
  grid-column: 1 / 3;
`;

export const Link = styled(GatsbyLink)`
  border-bottom: 1px dashed var(--light-gray);
  padding-bottom: 10px;
  color: var(--dark-gray);

  &:nth-last-child(1) {
    border-bottom: none;
  }
`;
