import React, { Component, useEffect } from 'react';

import { StrapiMediaFileInterface } from 'interfaces/interfaces';

import { Container } from 'UI';
import Slider from 'UI/Slider';

import { Section, Video, Title, Image } from './styles';

interface BannerInterface {
  baseUrl?: string;
  images?: StrapiMediaFileInterface[];
  bannerText?: string;
}

export class Banner extends Component<BannerInterface> {
  /*
  componentDidMount(){
    if (typeof window !== undefined) {
      const videos = document.querySelectorAll('video[id^="bannerVideo"]');

      videos?.forEach((item) => {
        item?.play();
      })
    }
  }
  */

  render() {
    const { bannerText = '', baseUrl = '', images = [] } = this.props;
    const intervals: number[] = images.map(({ localFile, mime = '' }) => {
      const duration = mime.includes('video')
        ? (localFile?.videoH264?.duration || 2) * 1000
        : 5000;

      return duration
    });

    return (
      <Section>
        <Title>
          <span>
          { bannerText }
          </span>
        </Title>

        <Slider intervals={intervals}>
          {images?.map((image, index) => {
            const { mime = '', localFile } = image;
            const url = localFile?.publicURL;

            return (
              mime.includes('video') ? (
                <Video
                  key={`slider_${url}`}
                  muted
                  loop
                  poster={localFile?.videoScreenshots?.[0].publicURL}
                  id={`bannerVideo_${index}`}
                >
                  <source
                    src={localFile?.videoH264?.path}
                    type="video/mp4"
                  />
                  <source
                    src={localFile?.videoVP9?.path}
                    type="video/webm"
                  />
                </Video>
              ) : (
                <Image
                  key={`slider_${url}_${index}`}
                  source={image}
                />
              )
            )}
          )}
        </Slider>
  
        <Container />
      </Section>
    )
  }
}

export default Banner;
