import styled from "styled-components"
import { Properties } from "csstype"

import { Image as ImageUI } from 'UI';

export const Section = styled.section<Properties>`
  background-image: url('${({ backgroundImage = '' }) => backgroundImage}');
  background-size: cover;
  background-position: center;
  width: 100%;
  height: 100vh;
  left: 0;
  top: 0;
  overflow: hidden;
`

export const Video = styled.video``;

export const Image = styled(ImageUI)`
  min-height: 100%;
  min-width: 100%;
  object-fit: cover;
`;

export const Title = styled.h1`
  position: absolute;
  color: var(--white);
  left: 10%;
  top: calc(50% - 50px);
  max-width: 580px;
  line-height: 1.0;
`;
