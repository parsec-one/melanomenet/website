import React, { FC, useState, useEffect, KeyboardEvent, useRef } from 'react';

import { StrapiMediaFileInterface } from 'interfaces/interfaces';

import {
  CloseFullscreenIcon,
  Container,
  Dot,
  DotsContainer,
  FullscreenIcon,
  Image,
  MediaContainer,
  MediaContainerBack,
  Wrapper,
} from './styles';

interface MediaSliderInterface {
  files: StrapiMediaFileInterface[];
  className?: string;
}

const MediaSlider: FC<MediaSliderInterface> = ({ files, className }) => {
  const [isFullScreen, setFullScreen] = useState(false);
  const [activeFile, setActiveFile] = useState(0);
  const [wrapperHeight, setHeight] = useState(0);

  const count = files.length;
  const imageRef = useRef(null);

  useEffect(() => {
    isFullScreen
      ? (document.body.style.overflow = 'hidden')
      : (document.body.style.overflow = 'unset');
  }, [isFullScreen]);

  const handleLeft = () => {
    if (activeFile === 0) {
      setActiveFile(count - 1);
    } else {
      setActiveFile(activeFile - 1);
    }
  }

  const handleOpen = () => {
    // @ts-ignore: Unreachable code error
    setHeight(imageRef?.current?.offsetHeight);
    setFullScreen(true);
  }

  const handleRight = () => {
    if (activeFile === count - 1) {
      setActiveFile(0);
    } else {
      setActiveFile(activeFile + 1);
    }
  }

  const handleKeyPress = (event: KeyboardEvent) => {
    const { key } = event;

    if (key === 'ArrowRight'){
      if (count > 1) handleRight()
    } else if (key === 'ArrowLeft') {
      if (count > 1) handleLeft()
    } else if (key === 'Enter') {
      setFullScreen(!isFullScreen)
    } else if (key === 'Escape') {
      setFullScreen(false)
    }
  }

  if (!Array.isArray(files)) return null;
  if (files.length === 0) return null;

  return (
    <Wrapper style={{ minHeight: `${wrapperHeight}px` }}>
      <Container
        isFullScreen={isFullScreen}
        onKeyDown={handleKeyPress}
        tabIndex={0}
        className={className}
        ref={imageRef}
      >
        {isFullScreen ? (
          <CloseFullscreenIcon onClick={() => setFullScreen(false)} />
        ) : (
          <FullscreenIcon onClick={() => handleOpen()} />
        )}
        {files.map((file, index) => (
          <MediaContainer
            isActive={activeFile === index}
            key={`media_${file.id}_${index}`}
            isFullScreen={isFullScreen}
          >
            <Image source={file} isFullScreen={isFullScreen} />
            {isFullScreen && (
              <MediaContainerBack onClick={() => setFullScreen(false)} />
            )}
          </MediaContainer>
        ))}
        { (count > 1) && (
          <DotsContainer isFullScreen={isFullScreen}>
            {files.map((file, index) => (
              <Dot
                key={`dot_${file.id}_${index}`}
                onClick={() => setActiveFile(index)}
                isActive={activeFile === index}
              />
            ))}
          </DotsContainer>
        ) }
      </Container>
    </Wrapper>
  )
}

export default MediaSlider;
