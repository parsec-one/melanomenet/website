import styled, { css } from 'styled-components';

import { FiMaximize2, FiMinimize2 } from 'react-icons/fi';

import { Image as ImageUI } from 'UI';
import { screen } from 'UI/variables'

interface ContainerInterface {
  isFullScreen: boolean;
}

export const Wrapper = styled.div`
  position: relative;
`;

export const Container = styled.div`
  position: relative;

  ${({ isFullScreen }: ContainerInterface) => isFullScreen && css`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100%;
    background: rgba(0, 0, 0, .8);
    z-index: 10;
    margin: 0 !important;
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: hidden;
    border-radius: 0;
  `}

  @media ${screen.tablet} {
    scroll-snap-type: x mandatory;  
    display: flex;
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    left: -30px;
    width: 100vw;
    background: var(--dark-gray);
  }
`;

export const Image = styled(ImageUI)`
  max-height: 500px;
  border-radius: 8px;
  box-shadow: 0 5px 10px rgba(0,0,0,.1);

  ${({ isFullScreen }: MediaContainerInterface) => isFullScreen && css`
    max-height: calc(100vh - 140px);
    position: relative;
    z-index: 1;
    border-radius: 0;
  `}

  @media ${screen.tablet} {
    border-radius: 0;
  }
`

interface MediaContainerInterface {
  isActive?: boolean;
  isFullScreen?: boolean;
}

export const MediaContainer = styled.div`
  position: relative;
  display: ${({ isActive }: MediaContainerInterface) => isActive ? 'flex' : 'none'};
  align-items: center;
  justify-content: center;

  @media ${screen.tablet} {
    display: flex !important;
    min-width: 100vw;
    scroll-snap-align: start;
  }

  ${({ isFullScreen }: MediaContainerInterface) => isFullScreen && css`
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    padding: 60px;
  `}
`;

export const MediaContainerBack = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 0;
`;

export const Dot = styled.div`
  width: 18px;
  height: 18px;
  border-radius: 50%;
  border: 4px solid;
  border-color: ${({ isActive }: MediaContainerInterface) => isActive ? 'var(--red)' : 'var(--light-gray)'};
  cursor: pointer;

  &:hover {
    background: var(--white);
    border-color: var(--dark-gray)
  }
`;

export const DotsContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px 0;


  @media ${screen.tablet} {
    display: none;
  }

  > *:not(:nth-child(1)) {
    margin-left: 10px;
  }

  ${({ isFullScreen }: ContainerInterface) => isFullScreen && css`
    position: absolute;
    bottom: 10px;
    left: 50%;
    transform: translateX(-50%);
  `}
`

export const FullscreenIcon = styled(FiMaximize2)`
  position: absolute;
  top: 20px;
  right: 20px;
  color: #000;
  z-index: 1;
  width: 30px;
  height: 30px;
  cursor: pointer;
  opacity: .4;

  @media ${screen.tablet} {
    display: none;
  }

  &:hover {
    opacity: 1;
  }
`;
export const CloseFullscreenIcon = styled(FiMinimize2)`
  position: absolute;
  top: 20px;
  right: 20px;
  color: var(--white);
  z-index: 1;
  width: 30px;
  height: 30px;
  cursor: pointer;
  opacity: .4;

  &:hover {
    opacity: 1;
  }
`;
