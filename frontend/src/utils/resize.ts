import { StrapiMediaFileInterface } from 'interfaces/interfaces';

type SizesType = 'large' | 'medium' | 'small' | number;

export const getResizedImageUrl = (
  url: string,
  size: SizesType, 
  siteURL = '',
): string => {
  const sizes = {
    large: 1600,
    medium: 1024,
    small: 480,
  }

  const width = (typeof size == 'number') ? size : sizes[size];

  return `${siteURL}/media/${width}/${url}`;
}

export const getFirstImageLink = (
  image?: StrapiMediaFileInterface[],
): string => (
  image?.[0]?.localFile?.publicURL || ''
);
