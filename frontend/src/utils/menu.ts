import {
  StrapiMenuItemInterface,
  GatsbyMenuItemInterface,
  StrapiPagesItemInterface,
  WikiTagInterface,
} from 'interfaces/interfaces';

interface NormalizeMenuInterface {
  menu: StrapiMenuItemInterface[];
}

export const normalizeMenu = (menu: StrapiMenuItemInterface[]): GatsbyMenuItemInterface[] => {
  const normalizePageItem = ({ name, slug, id }: WikiTagInterface): StrapiMenuItemInterface  => ({
    id: id,
    link: `/${slug}`,
    name: name,
    position: {
      isFooter: true,
      isHeader: true,
      isMobile: false,
    }
  });

  const normalizeSubMenu = (submenu: StrapiMenuItemInterface[], pages: StrapiPagesItemInterface[]): StrapiMenuItemInterface[] => {
    const menuItem: StrapiMenuItemInterface[] = [];

    submenu.forEach((item) => menuItem.push(item));

    const pageLinks: Array<WikiTagInterface[]> = pages.map((pageItem: StrapiPagesItemInterface) => {
      return Object.keys(pageItem).map((key) => pageItem[key]);
    });

    if (pageLinks && Array.isArray(pageLinks)) {
      pageLinks.flat(2).forEach((item: WikiTagInterface) => {
        menuItem.push(normalizePageItem(item))
      })
    }

    return menuItem;
  }

  return menu.map(({ pages = [], submenu = [], ...props }) => ({
    ...props,
    submenu: (submenu.length > 0 || pages.length > 0)
      ? normalizeSubMenu(submenu, pages)
      : undefined,
  }));
}
