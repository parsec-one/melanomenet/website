export const getDateText = (dateTime: string): string => {
  const date = new Date(dateTime);
  const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' };
  return date?.toLocaleDateString('ru-RU', dateOptions) || dateTime;
}

export const getDateDescription = (dateTime: string): string => {
  const date = new Date(dateTime);
  const dateOptions = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  };
  return date?.toLocaleDateString('ru-RU', dateOptions) || dateTime;
}
