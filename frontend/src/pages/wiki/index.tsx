import React, { FC } from 'react'
import { PageProps, StaticQuery, graphql } from 'gatsby'

import { AllWikiTagsInterface, PageInterface, SiteInfoInterface } from 'interfaces/interfaces'

import { Container, Flex, Section } from 'UI'
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs'

import { PageHeader, Layout, WikiList } from 'components'

interface IndexPageInterface {
  allStrapiTags: AllWikiTagsInterface;
  strapiWikiPage: PageInterface;
  strapiSiteInfo: SiteInfoInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        allStrapiTags(
          limit: 6
        ) {
          edges {
            node {
              name
              slug
              wikis {
                title
                description
                slug
                image {
                  localFile {
                    childImageSharp {
                      fluid(maxWidth: 400) {
                        src
                      }
                    }
                  }
                }
              }
            }
          }
          totalCount
        }

        strapiWikiPage {
          meta {
            title
            description
          }
          background {
            localFile {
              childImageSharp {
                fluid(
                  maxWidth: 640
                ) {
                  src
                  srcSet
                  srcSetWebp
                }
              }
              publicURL
            }
            alternativeText
            caption
          }
        }

        strapiSiteInfo {
          previewImage {
            childImageSharp {
              resize(width: 1920) {
                src
              }
            }
          }
        }
      }`
    }

    render={({ allStrapiTags, strapiWikiPage, strapiSiteInfo }: IndexPageInterface) => {
      const { edges } =  allStrapiTags;
      const { meta: { title } } = strapiWikiPage;

      return (
        <Layout
          pageInfo={strapiWikiPage}
          path={path}
          type="website"
        >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            <Flex column justifyContent="space-between">
              {edges.map(({ node: { name, wikis = [] } }, index) => (wikis.length > 0) && (
                <Section key={`wiki_${name}_${index}`} title={name}>
                  <WikiList articles={wikis} siteInfo={strapiSiteInfo} />
                </Section>
              ))}
            </Flex>
          </Container>
        </Layout>
      )}
    }
  />
)

export default IndexPage
