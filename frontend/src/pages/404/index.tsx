import React, { FC } from 'react';
import { Link, StaticQuery, graphql, PageProps } from 'gatsby';

import { PageInterface } from 'interfaces/interfaces';

import { Section, Container, FullHeight, Flex } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';
import { Layout } from 'components';

interface NotFoundPageInterface {
  strapiNotFound: PageInterface;
}

const NotFoundPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiNotFound {
          meta {
            title
            description
          }
        }
      }`
    }

    render={({ strapiNotFound }: NotFoundPageInterface) => (
      <Layout
        pageInfo={strapiNotFound}
        path={path}
      >
        <FullHeight>
          <Section>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>404</BreadcrumbsItem>
            </Breadcrumbs>
            <Container justifyContent="center">
              <Flex column space={10}>
                <Link to="/">
                  <h1 style={{ fontSize: '120px' }}>404</h1>
                </Link>
                <h2>{ strapiNotFound.meta.title }</h2>
                <Link to="/">вернуться на главную</Link>
              </Flex>
            </Container>
          </Section>
        </FullHeight>
      </Layout>
    )}
  />
)

export default NotFoundPage
