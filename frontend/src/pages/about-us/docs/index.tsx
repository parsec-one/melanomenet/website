import React, { FC } from 'react';
import { StaticQuery, graphql, PageProps } from 'gatsby';

import { PageInterface } from 'interfaces/interfaces';

import { Section, Container, Flex, SingleFile, Grid } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { PageHeader, Layout, ContactPanel } from 'components';

interface DocsPageInterface {
  strapiDocs: PageInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiDocs {
          meta {
            title
            description
          }
          files {
            id
            localFile {
              publicURL
              name
            }
            name
            caption
            ext
          }
        }
      }`
    }

    render={({ strapiDocs }: DocsPageInterface) => {
      const { meta, files } = strapiDocs;
      const { title } = meta;

      return (
        <Layout
          pageInfo={strapiDocs}
          path={path}
          type="website"
        >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to="/about-us">О нас</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            <Flex space={30} justifyContent="space-between" alignItems="stretch">
              <Section title={title}>
                <Grid space={10}>
                  {files && files.map(({ id, name = '', caption = '', ext = '', localFile }) => (
                    <SingleFile
                      key={`docsFile_${id}`}
                      name={name}
                      caption={caption}
                      url={localFile?.publicURL || ''}
                      ext={ext}
                    />
                  ))}
                </Grid>
              </Section>
              <ContactPanel />
            </Flex>
          </Container>
        </Layout>
      )}
    }
  />
)

export default IndexPage
