import React, { FC } from 'react';
import { StaticQuery, graphql, PageProps } from 'gatsby';

import { PageInterface, BoardTeamsInterface } from 'interfaces/interfaces';

import { Section, Container, Flex, Paragraph, Grid } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { PageHeader, Layout, BoardMemberCard, ContactUs } from 'components';

interface AboutUsPageInterface {
  strapiSpecialistsPage: PageInterface;
  allStrapiBoardTeams: BoardTeamsInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiSpecialistsPage {
          meta {
            title
            description
          }
          content
        }

        allStrapiBoardTeams(filter: {isDoctor: {eq: true}}) {
          nodes {
            id
            name
            content
            description
            slug
            image {
              ...StrapiSingleFileFragment
            }
            files {
              ...StrapiSingleFileFragment
            }
          }
        }
      }`
    }

    render={({ strapiSpecialistsPage, allStrapiBoardTeams }: AboutUsPageInterface) => {
      const { content = '', meta } = strapiSpecialistsPage;
      const { title } = meta;

      const { nodes } = allStrapiBoardTeams;

      return (
        <Layout
          pageInfo={strapiSpecialistsPage}
          path={path}
          type="website"
          >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to="/about-us">О нас</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            <Flex space={30} justifyContent="space-between" alignItems="stretch">
              <Section title={title}>
                <Paragraph isMarkdown text={content} />
                <Grid space={30} gridTemplateColumns="repeat(auto-fill, minmax(300px, auto))">
                  {nodes && nodes.map(({ id, name, description, slug, image }) => (
                    <BoardMemberCard
                      key={`boardMember_${id}`}
                      name={name}
                      description={description}
                      slug={slug}
                      image={image}
                    />
                  ))}
                </Grid>
              </Section>
            </Flex>
          </Container>

          <ContactUs />
        </Layout>
      )}
    }
  />
);

export default IndexPage
