import React, { FC } from 'react';
import { StaticQuery, graphql, PageProps } from 'gatsby'

import { PageInterface } from 'interfaces/interfaces'

import { Section, Container, Flex, Paragraph } from 'UI'
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs'

import { PageHeader, Layout, ContactPanel } from 'components'

interface AboutUsPageInterface {
  strapiAboutUs: PageInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiAboutUs {
          content
          meta {
            title
            description
          }
        }
      }`
    }

    render={({ strapiAboutUs }: AboutUsPageInterface) => {
      const { content = '', meta } = strapiAboutUs;
      const { title } = meta;

      return (
        <Layout
          pageInfo={strapiAboutUs}
          path={path}
          type="website"
          >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            <Flex space={30} justifyContent="space-between" alignItems="stretch">
              <Section title={title}>
                <Paragraph isMarkdown text={content} />
              </Section>
              <ContactPanel />
            </Flex>
          </Container>
        </Layout>
      )}
    }
  />
)

export default IndexPage
