import React, { FC } from 'react';
import { StaticQuery, graphql, PageProps } from 'gatsby';

import { PageInterface, ProgramsInterface } from 'interfaces/interfaces';

import { Section, Container, Flex, Paragraph, Grid } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import { PageHeader, Layout, ProgramCard, ContactUs } from 'components';

interface ProgramsPageInterface {
  strapiProgramPage: PageInterface;
  allStrapiProgram: ProgramsInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiProgramPage {
          meta {
            title
            description
          }
          content
          background {
            ...StrapiSingleFileFragment
          }
        }

        allStrapiProgram {
          nodes {
            id
            name
            content
            description
            slug
            image {
              ...StrapiSingleFileFragment
            }
            files {
              ...StrapiSingleFileFragment
            }
          }
        }
      }`
    }

    render={({ strapiProgramPage, allStrapiProgram }: ProgramsPageInterface) => {
      const { content = '', meta } = strapiProgramPage;
      const { title } = meta;

      const { nodes } = allStrapiProgram;

      return (
        <Layout
          pageInfo={strapiProgramPage}
          path={path}
          type="website"
          backgroundColor="var(--white)"
        >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to="/about-us">О нас</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            <Flex space={30} justifyContent="space-between" alignItems="stretch">
              <Section title={title}>
                <Paragraph isMarkdown text={content} />
                <Grid space={30} gridTemplateColumns="repeat(auto-fit, minmax(300px, auto))">
                  {nodes && nodes.map(({ id, name, description, slug, image }) => (
                    <ProgramCard
                      key={`boardMember_${id}`}
                      name={name}
                      description={description}
                      slug={`programs/${slug}`}
                      image={image}
                    />
                  ))}
                </Grid>
              </Section>
            </Flex>
          </Container>

          <ContactUs />
        </Layout>
      )}
    }
  />
);

export default IndexPage
