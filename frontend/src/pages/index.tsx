import React, { FC } from 'react'
import { PageProps, StaticQuery, graphql, Link } from 'gatsby'

import {
  AllStrapiArticlesInterface,
  AllStrapiWikisInterface,
  BoardTeamsInterface,
  PageInterface,
  SiteInfoInterface,
} from 'interfaces/interfaces'

import {
  Container,
  Flex,
  Grid,
  Pagination,
  Paragraph,
  Section
} from 'UI'

import {
  Banner,
  BoardMemberWidget,
  CardDescription,
  ContactPanel,
  ContactUs,
  Layout,
  Map,
  NewsList,
  SliderCards,
  WikiList,
} from 'components'

interface HomePageInterface extends PageInterface {
  bannerText: string;
}

interface IndexPageInterface {
  allStrapiArticles: AllStrapiArticlesInterface;
  allStrapiBoardTeams: BoardTeamsInterface;
  allStrapiWikis: AllStrapiWikisInterface;
  strapiAboutUs: PageInterface;
  strapiHomePage: HomePageInterface;
  strapiSiteInfo: SiteInfoInterface;
  strapiBoardPage: PageInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        allStrapiArticles(
          sort: { fields: [created_at], order: DESC }
          limit: 6
        ) {
          edges {
            node {
              title
              description
              image {
                ...StrapiSingleFileFragment
              }
              created_at
              slug
            }
          }
          totalCount
        }

        strapiHomePage {
          bannerText
          meta {
            title
            description
          }
          background {
            mime
            alternativeText
            caption
            ext
            localFile {
              publicURL
              childImageSharp {
                fluid(
                  maxWidth: 640
                ) {
                  src
                  srcSet
                  srcSetWebp
                }
              }
              videoScreenshots(width: 1600) {
                publicURL
              }
              videoH264(maxHeight: 720) {
                path
                duration
              }
              videoVP9(maxHeight: 720) {
                path
                duration
              }
            }
          }
        }

        strapiAboutUs {
          meta {
            title
            description
          }
          content
        }

        strapiSiteInfo {
          site_url
          previewImage {
            childImageSharp {
              resize(width: 1920) {
                src
              }
            }
          }
        }

        allStrapiBoardTeams(
          limit: 6
        ) {
          nodes {
            id
            name
            content
            description
            slug
            isPresident
            image {
              ...StrapiSingleFileFragment
            }
          }
        }

        strapiBoardPage {
          content
        }

        allStrapiWikis(sort: {fields: [created_at], order: DESC}, limit: 12) {
          nodes {
            title
            description
            tags {
              name
              slug
            }
            image {
              ...StrapiSingleFileFragment
            }
            created_at
            slug
          }
        }
      }`
    }

    render={({
      allStrapiArticles,
      allStrapiBoardTeams,
      allStrapiWikis,
      strapiAboutUs,
      strapiBoardPage,
      strapiHomePage,
      strapiSiteInfo,
    }: IndexPageInterface) => {
      const { background, meta, bannerText } = strapiHomePage;
      const { edges, totalCount } =  allStrapiArticles;
      const { content = '', meta: aboutUsMeta } =  strapiAboutUs;
      const { site_url } = strapiSiteInfo;
      const newsLimit = 6;

      const opengraph = {
        meta,
      }

      const master = allStrapiBoardTeams?.nodes?.find(({ isPresident }) => isPresident);

      return (
        <Layout
          pageInfo={opengraph}
          path={path}
          type="website"
        >
          <Banner
            bannerText={bannerText}
            baseUrl={site_url}
            images={background}
          />

          <Container>
            <Flex space={30} justifyContent="space-between">
              <Flex column style={{ width: '100%' }}>
                <Section title="Новости" url="/news">
                  <NewsList articles={edges} siteInfo={strapiSiteInfo} />

                  {(totalCount > newsLimit) && (
                    <Pagination
                      total={totalCount}
                      step={1}
                      perPage={newsLimit}
                      pageUrl="news"
                    />
                  )}

                  <Link to="/news" title="Все новости">Все новости</Link>
                </Section>
              </Flex>

              <ContactPanel style={{ marginTop: '-520px' }} />
            </Flex>
          </Container>

          <Container>
            <Section title="О меланоме" url="/wiki">
              <Flex column justifyContent="space-between">
                <WikiList articles={allStrapiWikis?.nodes} siteInfo={strapiSiteInfo} />
              </Flex>
            </Section>
          </Container>

          {master && (
            <BoardMemberWidget
              name={master.name}
              image={master.image}
              slug={master.slug}
              description={master.description}
            />
          )}

          <Container style={{
            maxWidth: '100%',
            overflow: 'hidden',
          }}>
            <Section>
              <Grid
                gridTemplateColumns="repeat(auto-fit, minmax(400px, 1fr))"
                gap="60px"
              >
                <SliderCards cards={allStrapiBoardTeams?.nodes} />

                <CardDescription
                  title="Попечительский совет"
                  url="/about-us/board"
                  text={strapiBoardPage?.content || ''}
                />
              </Grid>
            </Section>
          </Container>

        <Container id="scrollPanel-bottom">
          <Section title={aboutUsMeta.title}>
            <Paragraph text={content} isMarkdown />
          </Section>
        </Container>

          <div>
            {(typeof window !== 'undefined') && <Map />}
          </div>

          <ContactUs />
        </Layout>
      )}
    }
  />
);

export const query = graphql`
  fragment StrapiSingleFileFragment on StrapiSingleFile {
    id
    alternativeText
    caption
    localFile {
      childImageSharp {
        fluid(
          maxWidth: 400
        ) {
          src
          srcSet
          srcSetWebp
        }
      }
      publicURL
    }
  }
`;

export default IndexPage
