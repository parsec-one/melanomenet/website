import React, { FC } from 'react';
import { StaticQuery, graphql, PageProps } from 'gatsby';

import { PageInterface } from 'interfaces/interfaces';

import { Section, Container, Paragraph } from 'UI';
import { Breadcrumbs, BreadcrumbsItem } from 'UI/Breadcrumbs';

import Contacts from 'components/ContactPanel/Contacts';
import { PageHeader, Layout, Map, ContactUs } from 'components';

interface AboutUsPageInterface {
  strapiContactsPage: PageInterface;
}

const IndexPage: FC<PageProps> = ({ path }) => (
  <StaticQuery
    query = {
      graphql`{
        strapiContactsPage {
          content
          meta {
            title
            description
          }
        }
      }`
    }

    render={({ strapiContactsPage }: AboutUsPageInterface) => {
      const { content = '', meta } = strapiContactsPage;
      const { title } = meta;

      return (
        <Layout
          pageInfo={strapiContactsPage}
          path={path}
          type="website"
          >
          <PageHeader />
          <Container>
            <Breadcrumbs>
              <BreadcrumbsItem to="/">Главная</BreadcrumbsItem>
              <BreadcrumbsItem to={path}>{ title }</BreadcrumbsItem>
            </Breadcrumbs>
            {content && (
              <Section>
                <Paragraph isMarkdown text={content} />
              </Section>
            )}
            <Section title={title}>
              <Contacts />
            </Section>
          </Container>

          <div>
            {(typeof window !== 'undefined') && <Map />}
          </div>

          <ContactUs />
        </Layout>
      )}
    }
  />
);

export default IndexPage
