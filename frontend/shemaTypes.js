/**
 * Date
 */

const DateType = `
  created_at(
    difference: String
    formatString: String
    fromNow: Boolean
    locale: String
  ): Date
  updated_at(
    difference: String
    formatString: String
    fromNow: Boolean
    locale: String
  ): Date
`;

const DefaultTypes = `
  type MetaType {
    id: Int
    title: String
    description: String
  }

  type TagsType {
    ${DateType}
    id: Int
    name: String
    slug: String
  }

  type StrapiMenusPosition {
    id: Int
    isHeader: Boolean
    isMobile: Boolean
    isFooter: Boolean
  }

  type SubmenuItemType {
    ${DateType}
    id: Int
    name: String
    link: String
    sort: Int
    position: StrapiMenusPosition
  }

  type MenuPagesItemType {
    tags: [TagsType]
  }

  type StrapiMenus implements Node {
    ${DateType}
    id: Int
    name: String
    link: String
    sort: Int
    position: StrapiMenusPosition
    submenu: [SubmenuItemType]
    pages: [MenuPagesItemType]
  }

  type FileLogoType {
    id: Int!
    publicURL: String
  }

  type StrapiSingleFile {
    ${DateType}
    id: Int
    name: String
    alternativeText: String
    caption: String
    width: Int
    height: Int
    hash: String
    ext: String
    mime: String
    size: Float
    url: String
    provider: String
    localFile: File
  }

  type SiteAnalytics {
    googleTagId: String
    googleAnalyticsId: String
  }

  type Location implements Node {
    id: Int!
    address: String
    link: String
  }
`;

/**
 * Collections
 */

const StrapiWikiItem = `
  ${DateType}
  id: Int!
  title: String
  content: String
  description: String
  slug: String
  tags: [TagsType]
  image: [StrapiSingleFile]
`;

const StrapiWikis = `
  ${StrapiWikiItem}
  tags: [TagsType]
`;

const StrapiArticles = `
  ${DateType}
  id: ID!
  title: String
  description: String
  text: String
  slug: String
  strapiId: Int
  image: [StrapiSingleFile]
`;

const StrapiTags = `
  id: ID!
  parent: Node
  children: [Node!]!
  internal: Internal!
  name: String
  slug: String
  ${DateType}
  wikis: [StrapiWikiItem]
  strapiId: Int
`;

const StrapiBoardTeams = `
${DateType}
  id: ID!
  parent: Node
  children: [Node!]!
  internal: Internal!
  name: String
  description: String
  image: [StrapiSingleFile]
  files: [StrapiSingleFile]
  content: String
  slug: String
  strapiId: Int
  isPresident: Boolean
  isBoard: Boolean
  isDoctor: Boolean
`;

const StrapiPrograms = `
${DateType}
  id: ID!
  parent: Node
  children: [Node!]!
  internal: Internal!
  name: String
  description: String
  image: [StrapiSingleFile]
  files: [StrapiSingleFile]
  content: String
  slug: String
  strapiId: Int
`;

/**
 * Pages
 */

const PageType = `
  ${DateType}
  meta: MetaType
  id: ID!
  strapiId: Int
`

const StrapiHomePage = `
  ${PageType}
  bannerText: String
  background: [StrapiSingleFile]
`;

const StrapiAboutUs = `
  ${PageType}
  content: String
`;

const StrapiNotFound = `
  ${PageType}
`;

const StrapiNewsPage = `
  ${PageType}
  background: [StrapiSingleFile]
`;

const StrapiWikipage = `
  ${PageType}
  background: [StrapiSingleFile]
`

const StrapiDocsPage = `
  ${PageType}
  files: [StrapiSingleFile]
  background: [StrapiSingleFile]
`

const StrapiContactsPage = `
  ${PageType}
  content: String
  background: [StrapiSingleFile]
`

const StrapiBoardPage = `
  ${PageType}
  content: String
  background: [StrapiSingleFile]
`

const StrapiProgramPage = `
  ${PageType}
  content: String
  background: [StrapiSingleFile]
`

const StrapiSpecialistsPage = `
  ${PageType}
  content: String
`

/**
 * Single types
 */

const StrapiFoundCreatorWidget = `
  content: String
  image: StrapiSingleFile
`

/**
 * Site info
 */

const StrapiSiteInfo = `
  ${DateType}
  id: ID!
  logoText: String
  site_url: String
  title: String
  logo: File
  previewImage: File
  favicon: File
  strapiId: Int
  analytics: SiteAnalytics
`;

// need finish
const StrapiContacts = `
  email: String
  phone: String
  location: Location
`

module.exports = {
  DefaultTypes,
  StrapiArticles,
  StrapiBoardTeams,
  StrapiPrograms,
  StrapiTags,
  StrapiWikiItem,
  StrapiWikis,
  // Pages
  StrapiAboutUs,
  StrapiBoardPage,
  StrapiContactsPage,
  StrapiDocsPage,
  StrapiHomePage,
  StrapiNewsPage,
  StrapiNotFound,
  StrapiProgramPage,
  StrapiSpecialistsPage,
  StrapiWikipage,
  // Single items
  StrapiFoundCreatorWidget,
  // Site info
  StrapiContacts,
  StrapiSiteInfo,
};
